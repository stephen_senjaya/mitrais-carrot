import React, {Component} from 'react';
import {
  Modal, 
  Text, 
  TouchableHighlight, 
  View,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { 
  Button, 
  FormLabel, 
  FormInput, 
  FormValidationMessage,
  Header,
  Icon,
  Avatar,
  List,
  ListItem,
} from 'react-native-elements';

import * as Constanta from '../../config/constanta';
import * as Utility from '../../config/utility';

export default class Transaction extends Component {
  state = {
    isLoading: true,
    user: null,
    transactions: null,

  };

  async componentDidMount(){
    let user = this.props.navigation.state.params.user;
    let type = this.props.navigation.state.params.type;

    let transactions = null;

    if(!type)
      transactions = await Utility.getData(Constanta.TRANSACTION_BY_USER_API_URL + user.id);
    else
      transactions = await Utility.getData(Constanta.TRANSACTION_API_URL + user.id + '/' + type);

    this.setState({ isLoading: false, user, transactions });
  }

  render() {
    const { isLoading, user, transactions } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: Constanta.COLOR_DARK_GRAY }} >
        {/* <Header
          statusBarProps={{ barStyle: 'light-content' }}
          // leftComponent={{ text: 'Login', style: { color: '#fff' } }}
          // centerComponent={{ text: 'Login', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: Constanta.COLOR_GRAY, height: 0, }}
          innerContainerStyles={{ justifyContent: 'space-around' }}
        /> */}

        <View style = {{ flex: 1, margin: 5, marginTop: 25,
          borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
          <ScrollView style={{flex: 1 }} >
            {
              isLoading ? 
                <ActivityIndicator size = 'large' 
                  color = { Constanta.COLOR_ORANGE } 
                  style = {{ marginTop: 30}} />
              : (
                <List containerStyle={{marginBottom: 20, flex: 1}} >
                  {
                    transactions && (
                      transactions.map((transaction, i) => (
                        <ListItem
                          key={i}
                          hideChevron={true}
                          title={
                            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                              <View style={{flexDirection: 'row', alignItems: 'center'}} >
                                <Icon 
                                  name={Utility.getIconName(transaction.type)}
                                  type={Utility.getIconType(transaction.type)}
                                  size={30} 
                                  color={Constanta.COLOR_DARK} 
                                  containerStyle={{width: 30}}
                                />
                                <View style={{flexDirection: 'column', flex: 1, marginLeft: 5, marginRight: 5}} >
                                  <Text style={{color: Constanta.COLOR_DARK, fontSize: 11, fontWeight: 'bold'}} >
                                    {transaction.description}
                                  </Text>
                                  <Text style={{color: Constanta.COLOR_DARK, fontSize: 9}} >
                                    {Utility.getFormattedDate(transaction.dateOccured)}
                                  </Text>
                                </View>
                                <Text style={{color: Constanta.COLOR_DARK, width: 40, fontSize: 11}} >
                                  {transaction.amount}
                                </Text>
                              </View>
                            </View>
                          }
                          containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
                        />
                      ))
                    )
                  }
                </List>
              )
            }
          </ScrollView>
        </View>

      </View>
    );
  }
}
