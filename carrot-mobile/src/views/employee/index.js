import React, {Component} from 'react';
import {
  Modal, 
  Text, 
  TouchableHighlight, 
  View,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import { 
  Button, 
  FormLabel, 
  FormInput, 
  FormValidationMessage,
  Header,
  Icon,
  Avatar,
  List,
  ListItem,
  Card,
  Badge,
} from 'react-native-elements';

import * as Constanta from '../../config/constanta';
import * as Utility from '../../config/utility';

export default class Employee extends Component {
  state = {
    isLoading: true,
    user: null,
    rewards: null,
    socials: null,
    achievements: null,
    selectedContent: null,
    modalVisible: false,
    isModalLoading: false,
    sendAmount: 5,

  };

  async componentDidMount(){
    let user = this.props.navigation.state.params.user;
    // let rewardUrl = Constanta.CONTENT_BY_TYPE_API_URL + Constanta.REWARD_TYPE + '/' + (user && user.groupId? user.groupId : '');
    // console.log(rewardUrl);
    let rewards = await Utility.getData(Constanta.CONTENT_BY_TYPE_API_URL + Constanta.REWARD_TYPE + '/' + (user && user.groupId ? user.groupId : ''));
    let socials = await Utility.getData(Constanta.CONTENT_BY_TYPE_API_URL + Constanta.SOCIAL_TYPE + '/' + (user && user.groupId ? user.groupId : ''));
    let achievements = null;
    if (user && user.id) {
      achievements = await Utility.getData(Constanta.CONTENT_PLUS_API_URL + Constanta.ACHIEVEMENT_TYPE + '/' + user.id);
    }

    this.setState({ isLoading: false, user, rewards, socials, achievements });

  }

  exchangeCarrotMenu = (selectedContent) => {
    this.setState({ selectedContent });
    this.toggleModalVisible();
  }

  toggleModalVisible = () => {
    this.setState({modalVisible: !this.state.modalVisible});
  }

  exchangeCarrot = async() => {
    const {selectedContent} = this.state;
    this.setState({isModalLoading: true});

    let transaction = {
      'senderId': this.state.user.id,
      'contentId': selectedContent.id,
      'amount': selectedContent.type == Constanta.REWARD_TYPE ? 
        selectedContent.carrot : this.state.sendAmount,
      'description': Utility.getType(selectedContent.type) + ': ' + selectedContent.name,
      'type': selectedContent.type,
    }

    let res = await Utility.sendData(Constanta.TRANSACTION_API_URL, transaction);

    this.setState({isModalLoading: false});

    if (res) {
      alert('Exchange success!');
      this.toggleModalVisible();
    } else {
      alert('Exchange failed!');
      // this.toggleModalVisible();
    }
  }

  amountChange = (amount) => {
    // console.log(amount);
    if(!isNaN(amount)){
      this.setState({ sendAmount: amount });
      // console.log(amount);
    }
  }

  render() {
    const { isLoading, user, rewards, socials, achievements, selectedContent, isModalLoading } = this.state;
    return (
      <View style={{ flex: 1, backgroundColor: Constanta.COLOR_DARK_GRAY }} >
        {/* <Header
          statusBarProps={{ barStyle: 'light-content' }}
          // leftComponent={{ text: 'Login', style: { color: '#fff' } }}
          // centerComponent={{ text: 'Login', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: Constanta.COLOR_GRAY, height: 0, }}
          innerContainerStyles={{ justifyContent: 'space-around' }}
        /> */}
        <Modal
          transparent = { true }
          visible = { this.state.modalVisible }
          onRequestClose={this.toggleModalVisible}
        >
          <View style = {{ flex: 1, justifyContent: 'center', alignItems: 'center',
            backgroundColor: Constanta.COLOR_TRANSPARANT }} >
            <View style = {{ width: 300, height: 350,
              backgroundColor: Constanta.COLOR_LIGHT_GRAY, borderRadius: 5 }} >

              <View style={{flex: 1}} >
                <FormLabel>Exchange to: {selectedContent ? selectedContent.name : 'Name'}</FormLabel>
                {
                  selectedContent && selectedContent.type == Constanta.REWARD_TYPE ? (
                    <View>
                      <FormLabel>Price: {selectedContent.carrot}</FormLabel>
                    </View>
                  ) : (
                    <View>
                      <FormLabel>Amount</FormLabel>
                      <FormInput 
                        onChangeText = { (amount) => this.amountChange(amount) } 
                      />
                    </View>
                  )
                }
              </View>
              {
                isModalLoading ? 
                  <ActivityIndicator size = 'large' 
                    color = { Constanta.COLOR_ORANGE } 
                    style = {{justifyContent: 'flex-end', marginBottom: 30 }} />
                : (
                  <View style={{justifyContent: 'flex-end', }} >
                    <Button 
                      title = 'EXCHANGE' 
                      onPress = { this.exchangeCarrot } 
                      backgroundColor = { Constanta.COLOR_ORANGE } 
                      borderRadius = { 5 }
                      buttonStyle = {{ }}
                    />
                    <Button 
                      title = 'Cancel' 
                      onPress = { this.toggleModalVisible } 
                      backgroundColor = { Constanta.COLOR_GRAY }
                      color = {Constanta.COLOR_ORANGE} 
                      borderRadius = { 5 }
                      buttonStyle = {{ marginBottom: 10, marginTop: 10 }}
                    />
                  </View>
                )
              }
            </View>
          </View>
        </Modal>

        <ScrollView style = {{ flex: 1, margin: 5, marginTop: 25,
          borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
          {
            isLoading ? 
              <ActivityIndicator size = 'large' 
                color = { Constanta.COLOR_ORANGE } 
                style = {{ marginTop: 30}} />
            : (
              <View>
                <List containerStyle={{marginTop: 0, marginBottom: 20, flex: 1,
                  borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
                  <Badge containerStyle={{ backgroundColor: Constanta.COLOR_GREEN}}>
                    <Text h2 style={{color: Constanta.COLOR_LIGHT_GRAY, fontWeight: 'bold'}} >
                      Reward
                    </Text>
                  </Badge>
                  {
                    rewards && (
                      rewards.map((content, i) => (
                        <Card
                          key={'reward' + i}
                          // title={content.name} 
                          image={content.picture ? {uri: content.picture} : require('../../assets/img/na.png')}
                          containerStyle={{backgroundColor: Constanta.COLOR_LIGHT_GRAY, borderRadius: 5}}
                          imageProps={{resizeMode: 'contain', resizeMethod: 'resize'}}
                        >
                          {/* <Text style={{color: Constanta.COLOR_DARK, fontWeight: 'bold'}} >
                            {content.maxCarrot ? content.maxCarrot : 0}
                          </Text> */}
                          <Button
                            title={content.name ? content.name : 'name'}
                            backgroundColor='rgba(0,0,0,0)'
                            color={Constanta.COLOR_DARK}
                            buttonStyle={{padding: 0, justifyContent: 'flex-start'}}
                          />
                          <Button
                            icon={{
                              name: 'euro', 
                              type: 'font-awesome', 
                              color: Constanta.COLOR_DARK 
                            }}
                            title={content.carrot ? content.carrot + '' : '-'}
                            backgroundColor='rgba(0,0,0,0)'
                            color={Constanta.COLOR_DARK}
                            textStyle={{marginLeft: 1, fontWeight: 'bold'}}
                            buttonStyle={{paddingLeft: 0, justifyContent: 'flex-start'}}
                          />
                          <Button
                            icon={{
                              name: 'add-shopping-cart', 
                              type: 'MaterialIcons', 
                              color: Constanta.COLOR_LIGHT_GRAY 
                            }}
                            onPress={() => this.exchangeCarrotMenu(content)}
                            title='EXCHANGE' 
                            backgroundColor={Constanta.COLOR_ORANGE}
                            color={Constanta.COLOR_LIGHT_GRAY}
                            // fontFamily='Lato'
                            buttonStyle={{borderRadius: 5}}
                          />
                        </Card>
                        // <ListItem
                        //   key={i}
                        //   roundAvatar
                        //   avatar={content.picture ? {uri: content.picture} : require('../../assets/img/reward.png')}
                        //   title={content.name}
                        //   subtitle={content.maxCarrot}
                        //   containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
                        // />
                      ))
                    )
                  }
                </List>

                <List containerStyle={{marginTop: 0, marginBottom: 20, flex: 1,
                  borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
                  <Badge containerStyle={{ backgroundColor: Constanta.COLOR_GREEN}}>
                    <Text h2 style={{color: Constanta.COLOR_LIGHT_GRAY, fontWeight: 'bold'}} >
                      Social Foundation
                    </Text>
                  </Badge>
                  {
                    socials && (
                      socials.map((content, i) => (
                        <Card
                          key={'social' + i}
                          // title={content.name} 
                          image={content.picture ? {uri: content.picture} : require('../../assets/img/na.png')}
                          containerStyle={{backgroundColor: Constanta.COLOR_LIGHT_GRAY, borderRadius: 5}}
                          imageProps={{resizeMode: 'contain', resizeMethod: 'resize'}}
                        >
                          {/* <Text style={{color: Constanta.COLOR_DARK, fontWeight: 'bold'}} >
                            {content.maxCarrot ? content.maxCarrot : 0}
                          </Text> */}
                          <Button
                            title={content.name ? content.name : 'name'}
                            backgroundColor='rgba(0,0,0,0)'
                            color={Constanta.COLOR_DARK}
                            buttonStyle={{padding: 0, justifyContent: 'flex-start'}}
                          />
                          <Button
                            icon={{
                              name: 'euro', 
                              type: 'font-awesome', 
                              color: Constanta.COLOR_DARK 
                            }}
                            title={content.carrot ? content.carrot + '' : '-'}
                            backgroundColor='rgba(0,0,0,0)'
                            color={Constanta.COLOR_DARK}
                            textStyle={{marginLeft: 1, fontWeight: 'bold'}}
                            buttonStyle={{paddingLeft: 0, justifyContent: 'flex-start'}}
                          />
                          <Button
                            icon={{
                              name: 'add-shopping-cart', 
                              type: 'MaterialIcons', 
                              color: Constanta.COLOR_LIGHT_GRAY 
                            }}
                            onPress={() => this.exchangeCarrotMenu(content)}
                            title='EXCHANGE' 
                            backgroundColor={Constanta.COLOR_ORANGE}
                            color={Constanta.COLOR_LIGHT_GRAY}
                            // fontFamily='Lato'
                            buttonStyle={{borderRadius: 5}}
                          />
                        </Card>
                        // <ListItem
                        //   key={i}
                        //   roundAvatar
                        //   avatar={content.picture ? {uri: content.picture} : require('../../assets/img/reward.png')}
                        //   title={content.name}
                        //   subtitle={content.maxCarrot}
                        //   containerStyle={{backgroundColor: Constanta.COLOR_GRAY}}
                        // />
                      ))
                    )
                  }
                </List>

                <List containerStyle={{marginTop: 0, marginBottom: 20, flex: 1,
                  borderRadius: 5, backgroundColor: Constanta.COLOR_GRAY }} >
                  <Badge containerStyle={{ backgroundColor: Constanta.COLOR_GREEN}}>
                    <Text h2 style={{color: Constanta.COLOR_LIGHT_GRAY, fontWeight: 'bold'}} >
                      Achievement
                    </Text>
                  </Badge>
                  {
                    achievements && (
                      achievements.map((content, i) => (
                        <Card
                          key={'achievement' + i}
                          // title={content.name} 
                          image={content.picture ? {uri: content.picture} : require('../../assets/img/na.png')}
                          containerStyle={{backgroundColor: Constanta.COLOR_LIGHT_GRAY, borderRadius: 5}}
                          imageProps={{resizeMode: 'contain', resizeMethod: 'resize'}}
                        >
                          <Button
                            title={content.name ? content.name : 'name'}
                            backgroundColor='rgba(0,0,0,0)'
                            color={Constanta.COLOR_DARK}
                            buttonStyle={{padding: 0, justifyContent: 'flex-start'}}
                          />
                          <Button
                            icon={{
                              name: 'euro', 
                              type: 'font-awesome', 
                              color: Constanta.COLOR_DARK 
                            }}
                            title={content.carrot ? content.carrot + '' : '-'}
                            backgroundColor='rgba(0,0,0,0)'
                            color={Constanta.COLOR_DARK}
                            textStyle={{marginLeft: 1, fontWeight: 'bold'}}
                            buttonStyle={{paddingLeft: 0, justifyContent: 'flex-start'}}
                          />
                          <Button
                            icon={{
                              name: 'add-shopping-cart', 
                              type: 'MaterialIcons', 
                              color: Constanta.COLOR_LIGHT_GRAY 
                            }}
                            // onPress={() => this.exchangeCarrotMenu(content)}
                            title={content.currentCarrot > 0 ? 'Accomplished' : 'Not Accomplished yet'}
                            backgroundColor={Constanta.COLOR_ORANGE}
                            color={Constanta.COLOR_LIGHT_GRAY}
                            // fontFamily='Lato'
                            buttonStyle={{borderRadius: 5}}
                          />
                        </Card>
                      ))
                    )
                  }
                </List>
              </View>
            )
          }
        </ScrollView>

      </View>
    );
  }
}
