import React, {Component} from 'react';
import {
  Modal, 
  Text, 
  TouchableHighlight, 
  View,
  Image
} from 'react-native';
import { 
  Button, 
  FormLabel, 
  FormInput, 
  FormValidationMessage,
  Header,
  Icon,
} from 'react-native-elements';

import * as Constanta from '../../config/constanta';
import * as Utility from '../../config/utility';

export default class SeniorManager extends Component {
  state = {
    // modalVisible: false,
  };

  // setModalVisible(visible) {
  //   this.setState({modalVisible: visible});
  // }

  render() {
    return (
      <View style={{flex: 1, }} >
        <Header
          statusBarProps={{ barStyle: 'light-content' }}
          // leftComponent={{ text: 'Login', style: { color: '#fff' } }}
          // centerComponent={{ text: 'Login', style: { color: '#fff' } }}
          outerContainerStyles={{ backgroundColor: '#BEA4D9' }}
          innerContainerStyles={{ justifyContent: 'space-around' }}
        />

        <View style={{flex: 1, }} >
          <Image 
            source = { require('../../assets/img/icon.png') }
            style = {{ width: 180, height: 180 }}
          />
        </View>
        <View style={{
          height: 30, justifyContent: 'flex-end', 
          backgroundColor: Constanta.BG_COLOR_PRIMARY
        }} >
          <View style={{flex: 1, justifyContent: 'center', margin: 'auto'}} >
            <View style={{flexDirection: 'row', justifyContent: 'space-around'}} >
              <View style={{}} >
                <Icon 
                  name='home'
                  color={Constanta.COLOR_PRIMARY}
                />
              </View>
              <View style={{}} >
                <Icon 
                  name='people'
                  color={Constanta.BG_COLOR_BASIC}
                />
              </View>
              <View style={{}} >
                <Icon 
                  name='cake'
                  color={Constanta.BG_COLOR_BASIC}
                />
              </View>
              <View style={{}} >
                <Icon 
                  name='notifications'
                  color={Constanta.BG_COLOR_BASIC}
                />
              </View>
              <View style={{}} >
                <Icon 
                  name='person'
                  color={Constanta.BG_COLOR_BASIC}
                  // onPress={}
                />
              </View>
            </View>
          </View>
          
        </View>
      </View>
    );
  }
}
