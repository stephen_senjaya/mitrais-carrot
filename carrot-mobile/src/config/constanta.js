import { Dimensions } from 'react-native';


// export const BASE_URL = 'http://192.168.43.186:8846/';
export const BASE_URL = 'https://carrot.mybluemix.net/';
export const API_URL = BASE_URL + 'api/';

export const CONTENT_API_URL = API_URL + 'content/';
export const CONTENT_BY_TYPE_API_URL = CONTENT_API_URL + 'type/'; //add type and groupId
export const CONTENT_PLUS_API_URL = CONTENT_API_URL + 'plus/';  //add type and userId

export const NOTIFICATION_API_URL = API_URL + 'notification/';
export const NOTIFICATION_BY_USER_API_URL = NOTIFICATION_API_URL + 'user/'; //add userId

export const TRANSACTION_API_URL = API_URL + 'transaction/';
export const TRANSACTION_BY_USER_API_URL = TRANSACTION_API_URL + 'user/'; //add userId

export const USER_API_URL = API_URL + 'user/';
export const USER_PLUS_API_URL = USER_API_URL + 'plus/';
export const USER_BY_SUPERVISOR_API_URL = USER_API_URL + 'supervisor/';  //add supervisorId
export const BIRTHDAY_USER_API_URL = USER_API_URL + 'birthday/';  //add int (1 for tommorow, -1 for yesterday)

export const SESSION_API_URL = API_URL + 'session/';
export const LOGIN_API_URL = SESSION_API_URL + 'login/';

export const UNSIGNED_UPLOAD_PRESET = 'lurfv21d';
export const UPLOAD_URL = 'https://api.cloudinary.com/v1_1/huzakerna/image/upload';

export const COLOR_LIGHT_GRAY = '#EEEEEE';
export const COLOR_GRAY = '#E1E1E1';
export const COLOR_DARK_GRAY = '#D2D2D2';
export const COLOR_DARK = '#A5A5A5';
export const COLOR_PURPLE = '#350065';
export const COLOR_ORANGE = '#FF5723';
export const COLOR_YELLOW = '#F7941E';
export const COLOR_GREEN = '#8CC63F';
export const BG_COLOR = '#13A69C';
export const COLOR_TRANSPARANT = 'rgba(0, 0, 0, 0.3)';

// export const STAFF_ROLE = 1;
// export const ADMIN_ROLE = 2;
//	export const UNKNOWN_ROLE = 3;
export const MANAGER_ROLE = 4;
export const EMPLOYEE_ROLE = 5;
export const ROOT_ADMIN_ROLE = 6;
//	export const STAKEHOLDER_ROLE = 7;
export const SENIOR_MANAGER_ROLE = 8;

export const ACHIEVEMENT_TYPE = 1;
export const REWARD_TYPE = 2;
export const SOCIAL_TYPE = 3;
export const SHARED_TYPE = 4;

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;