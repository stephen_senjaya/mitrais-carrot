package com.mitrais.carrot.repository;

import com.mitrais.carrot.model.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface TransactionRepository extends MongoRepository<Transaction, String> {

//    List<Transaction> findByContentIdAndDateOccuredGreaterThanAndDateOccuredLowerThan(String contenntId, Date dateOccuredStart, Date dateOccuredEnd);
//    @Query()
//    List<Transaction> findByContent(@Param("contentId") String contentId, @Param("start") Date start, @Param("end") Date end);
}
