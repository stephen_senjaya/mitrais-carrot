package com.mitrais.carrot.dal;

import com.mitrais.carrot.model.ManagerGroup;

import java.util.List;

public interface ManagerGroupDal {

	List<ManagerGroup> findAll();

	List<ManagerGroup> getGroupsBySupervisor(String supervisorId);

	ManagerGroup getGroupById(String id);

	ManagerGroup getGroupByName(String name);

	ManagerGroup addEditGroup(ManagerGroup group);
}
