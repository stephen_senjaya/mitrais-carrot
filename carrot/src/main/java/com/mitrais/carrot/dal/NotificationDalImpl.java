package com.mitrais.carrot.dal;

import com.mitrais.carrot.config.Constanta;
import com.mitrais.carrot.model.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

@Repository
public class NotificationDalImpl implements NotificationDal {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<Notification> findAll() {
		Query query = new Query();
//		query.addCriteria(Criteria.where("active").is(true));
		return mongoTemplate.find(query, Notification.class);
	}

	@Override
	public Notification getById(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, Notification.class);
	}

	@Override
	public List<Notification> getByType(int type) {
		Query query = new Query();
		query.addCriteria(Criteria.where("type").is(type));
		return mongoTemplate.find(query, Notification.class);
	}

	@Override
	public List<Notification> getByUser(String userId) {
		Query query = new Query();
		Criteria criteria = new Criteria().orOperator(
				Criteria.where("userId").exists(false),
				Criteria.where("userId").is(userId),
				Criteria.where("type").is(Constanta.BIRTHDAY_NOTIF)
		);
		query.addCriteria(criteria);
		return mongoTemplate.find(query, Notification.class);
	}

	@Override
	public List<Notification> getByTypeAndDate(int type, Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateText = sdf.format(date);
		System.out.println(dateText);
		try {
			Date minDate = sdf.parse(dateText);
			Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));

			Criteria criteria = new Criteria().andOperator(
					Criteria.where("type").is(type),
					Criteria.where("creationDate").gt(minDate),
					Criteria.where("creationDate").lt(maxDate)
			);
			Query query = new Query();
			query.addCriteria(criteria);
			return mongoTemplate.find(query, Notification.class);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Notification addEdit(Notification notification) {
		mongoTemplate.save(notification);
		return notification;
	}

	@Override
	public boolean isDuplicated(Notification notification) {
		Query query = new Query();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateText = sdf.format(notification.getCreationDate());
		System.out.println(dateText);
		try {
			Date minDate = sdf.parse(dateText);
			Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));

			Criteria criteria = new Criteria().andOperator(
					Criteria.where("userId").is(notification.getUserId()),
					Criteria.where("type").is(notification.getType()),
					Criteria.where("creationDate").gt(minDate),
					Criteria.where("creationDate").lt(maxDate)
			);
			query.addCriteria(criteria);
			List<Notification> notifications = mongoTemplate.find(query, Notification.class);
			if (!notifications.isEmpty() && notifications != null)
				return true;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return false;
	}

}
