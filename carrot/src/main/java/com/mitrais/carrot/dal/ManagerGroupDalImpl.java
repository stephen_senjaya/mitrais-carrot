package com.mitrais.carrot.dal;

import com.mitrais.carrot.model.ManagerGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ManagerGroupDalImpl implements ManagerGroupDal {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<ManagerGroup> findAll() {;
		Query query = new Query();
		query.addCriteria(Criteria.where("active").is(true));
		return mongoTemplate.find(query, ManagerGroup.class);
	}

	@Override
	public List<ManagerGroup> getGroupsBySupervisor(String supervisorId) {
		Query query = new Query();
		query.addCriteria(Criteria.where("seniorManagerId").is(supervisorId));
		return mongoTemplate.find(query, ManagerGroup.class);
	}

	@Override
	public ManagerGroup getGroupById(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, ManagerGroup.class);
	}

	@Override
	public ManagerGroup getGroupByName(String name) {
		Query query = new Query();
		query.addCriteria(Criteria.where("name").is(name));
		return mongoTemplate.findOne(query, ManagerGroup.class);
	}

	@Override
	public ManagerGroup addEditGroup(ManagerGroup group) {
		mongoTemplate.save(group);
		return group;
	}
}
