package com.mitrais.carrot.dal;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.mitrais.carrot.config.Constanta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;

import com.mitrais.carrot.model.User;

@Repository
public class UserDalImpl implements UserDal {
	
	@Autowired
	private MongoTemplate mongoTemplate;

	@Override
	public List<User> findAll() {
		Query query = new Query();

		query.addCriteria(Criteria.where("active").is(true));
		return mongoTemplate.find(query, User.class);
	}

//	@Override
//	public List<User> getBirthdayUsers() {
//		Query query = new Query();
//
//		Criteria cAdmin = new Criteria().orOperator(
//				Criteria.where("admin").exists(false),
//				Criteria.where("admin").is(false)
//		);
//
//		Criteria criteria = new Criteria().andOperator(
//				cAdmin, Criteria.where("role").is(Constanta.MANAGER_ROLE), Criteria.where("active").is(true)
//		);
//
//		query.addCriteria(criteria);
//		return mongoTemplate.find(query, User.class);
//	}

	@Override
	public List<User> getManagers() {
		Query query = new Query();

		Criteria cAdmin = new Criteria().orOperator(
				Criteria.where("admin").exists(false),
				Criteria.where("admin").is(false)
		);

		Criteria criteria = new Criteria().andOperator(
				cAdmin, Criteria.where("role").is(Constanta.MANAGER_ROLE), Criteria.where("active").is(true)
		);

		query.addCriteria(criteria);
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> getSeniorManagers() {
		Query query = new Query();

		Criteria cAdmin = new Criteria().orOperator(
				Criteria.where("admin").exists(false),
				Criteria.where("admin").is(false)
		);

		Criteria criteria = new Criteria().andOperator(
				cAdmin, Criteria.where("role").is(Constanta.SENIOR_MANAGER_ROLE), Criteria.where("active").is(true)
		);

		query.addCriteria(criteria);
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> getEmployees() {
		Query query = new Query();

		Criteria cAdmin = new Criteria().orOperator(
				Criteria.where("admin").exists(false),
				Criteria.where("admin").is(false)
		);

		Criteria criteria = new Criteria().andOperator(
				cAdmin, Criteria.where("role").is(Constanta.EMPLOYEE_ROLE), Criteria.where("active").is(true)
		);

		query.addCriteria(criteria);
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> getAdmin() {
		Query query = new Query();

		Criteria criteria = new Criteria().andOperator(
				Criteria.where("admin").is(true), Criteria.where("active").is(true)
		);

		query.addCriteria(criteria);
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> getUngroupUsers() {
		Query query = new Query();

		Criteria cAdmin = new Criteria().orOperator(
				Criteria.where("admin").exists(false),
				Criteria.where("admin").is(false)
		);
		Criteria cGroupMember = new Criteria().orOperator(
				Criteria.where("groupMember").exists(false),
				Criteria.where("groupMember").is(false)
		);
		Criteria cRole = new Criteria().orOperator(
				Criteria.where("role").is(Constanta.MANAGER_ROLE),
				Criteria.where("role").is(Constanta.EMPLOYEE_ROLE)
		);

		Criteria criteria = new Criteria().andOperator(
				cAdmin, cGroupMember, cRole, Criteria.where("active").is(true)
		);
		query.addCriteria(criteria);
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> getUngroupEmployees() {
		Query query = new Query();

		Criteria cAdmin = new Criteria().orOperator(
				Criteria.where("admin").exists(false),
				Criteria.where("admin").is(false)
		);
		Criteria cGroupMember = new Criteria().orOperator(
				Criteria.where("groupMember").exists(false),
				Criteria.where("groupMember").is(false)
		);
		Criteria cRole = new Criteria().orOperator(
//				Criteria.where("role").is(Constanta.MANAGER_ROLE),
				Criteria.where("role").is(Constanta.EMPLOYEE_ROLE)
		);

		Criteria criteria = new Criteria().andOperator(
				cAdmin, cGroupMember, cRole, Criteria.where("active").is(true)
		);
		query.addCriteria(criteria);
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> getUngroupManagers() {
		Query query = new Query();

		Criteria cAdmin = new Criteria().orOperator(
				Criteria.where("admin").exists(false),
				Criteria.where("admin").is(false)
		);
		Criteria cGroupMember = new Criteria().orOperator(
				Criteria.where("groupMember").exists(false),
				Criteria.where("groupMember").is(false)
		);
		Criteria cRole = new Criteria().orOperator(
				Criteria.where("role").is(Constanta.MANAGER_ROLE)
//				Criteria.where("role").is(Constanta.EMPLOYEE_ROLE)
		);

		Criteria criteria = new Criteria().andOperator(
				cAdmin, cGroupMember, cRole, Criteria.where("active").is(true)
		);
		query.addCriteria(criteria);
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> getUsersBySupervisor(String supervisorId) {
		Query query = new Query();

		Criteria criteria = new Criteria().andOperator(
				Criteria.where("supervisorId").is(supervisorId), Criteria.where("active").is(true)
		);
		query.addCriteria(criteria);
		return mongoTemplate.find(query, User.class);
	}

	@Override
	public List<User> getUsersByGroup(String groupId) {
		Query query = new Query();

		Criteria criteria = new Criteria().andOperator(
				Criteria.where("groupId").is(groupId), Criteria.where("active").is(true)
		);
		query.addCriteria(criteria);
		return mongoTemplate.find(query, User.class);
	}

//	@Override
//	public List<User> getBirthdayUsers() {
//		Query query = new Query();
//		query.addCriteria(Criteria.where("dob").is(new Date())
//				.andOperator(Criteria.where("active").is(true)));
//		return mongoTemplate.find(query, User.class);
//	}

	@Override
	public User getUserById(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		return mongoTemplate.findOne(query, User.class);
	}

	@Override
	public User getUserByEmail(String email) {
		Query query = new Query();
		query.addCriteria(Criteria.where("email").is(email.toLowerCase()));
		return mongoTemplate.findOne(query, User.class);
	}

	@Override
	public User addEditUser(User user) {
		try {
			mongoTemplate.save(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		// Now, user object will contain the ID as well
		return user;
	}

	@Override
	public List<User> getByBirthdate(Date date) {

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		String dateText = sdf.format(date);
		System.out.println(dateText);
		try {
			Date minDate = sdf.parse(dateText);
			Date maxDate = new Date(minDate.getTime() + TimeUnit.DAYS.toMillis(1));

			Criteria criteria = new Criteria().andOperator(
					Criteria.where("active").is(true),
					Criteria.where("dob").gt(minDate),
					Criteria.where("dob").lt(maxDate)
			);
			Query query = new Query();
			query.addCriteria(criteria);
			return mongoTemplate.find(query, User.class);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Object getAllUserSettings(String id) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		User user = mongoTemplate.findOne(query, User.class);
		return user != null ? user.getSettings() : "User not found.";
	}

	@Override
	public String getUserSetting(String id, String key) {
		Query query = new Query();
		query.fields().include("settings");
		query.addCriteria(Criteria.where("id").is(id).andOperator(Criteria.where("settings." + key).exists(true)));
		User user = mongoTemplate.findOne(query, User.class);
		return user != null ? user.getSettings().get(key) : "Not found.";
	}

	@Override
	public String addUserSetting(String id, String key, String value) {
		Query query = new Query();
		query.addCriteria(Criteria.where("id").is(id));
		User user = mongoTemplate.findOne(query, User.class);
		if (user != null) {
			user.getSettings().put(key, value);
			mongoTemplate.save(user);
			return "Key added.";
		} else {
			return "User not found.";
		}
	}
}
