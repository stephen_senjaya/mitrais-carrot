package com.mitrais.carrot.dal;

import com.mitrais.carrot.model.Group;
import com.mitrais.carrot.model.User;

import java.util.List;

public interface GroupDal {

	List<Group> findAll();

	List<Group> getGroupsBySupervisor(String supervisorId);

	Group getGroupById(String id);

	Group getGroupByName(String name);

	Group addEditGroup(Group group);
}
