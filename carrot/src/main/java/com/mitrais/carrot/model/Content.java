package com.mitrais.carrot.model;

import org.springframework.data.annotation.Id;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Content {

    @Id
    private String id;
    private int type;
    private String name;
    private String description;
    private String picture;
    private int role;
    private Date expired;

    /**
     * price for exchange carrot to reward
     * total carrot needed to social
     * amount of achievement
     */
    private int carrot;
    /**
     * for storing amount of receiving carrot now with cutted if more than carrot (social)
     * for storing limit of redeemed (reward)
     * > 0 mean logged in user has accomplished it
     */
    private int currentCarrot;
    /**
     * for storing amount of receiving carrot total (social)
     * for storing times redeemed reward (reward)
     * for storing achieve each month (achievement)
     */
    private int totalUsage;

    private List<ContentChange> changes = new ArrayList<>();
    private List<String> groupsId = new ArrayList<>();
    private Date creationDate = new Date();
    private boolean active = true;
    private boolean deleted = false;

    private List<Group> groups = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    public int getCarrot() {
        return carrot;
    }

    public void setCarrot(int carrot) {
        this.carrot = carrot;
    }

    public int getCurrentCarrot() {
        return currentCarrot;
    }

    public void setCurrentCarrot(int currentCarrot) {
        this.currentCarrot = currentCarrot;
    }

    public int getTotalUsage() {
        return totalUsage;
    }

    public void setTotalUsage(int totalUsage) {
        this.totalUsage = totalUsage;
    }

    public List<ContentChange> getChanges() {
        return changes;
    }

    public void setChanges(List<ContentChange> changes) {
        this.changes = changes;
    }

    public List<String> getGroupsId() {
        return groupsId;
    }

    public void setGroupsId(List<String> groupsId) {
        this.groupsId = groupsId;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }
}
