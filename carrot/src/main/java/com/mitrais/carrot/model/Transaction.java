package com.mitrais.carrot.model;

import org.springframework.data.annotation.Id;

import java.util.Date;

public class Transaction {

    @Id
    private String id;
    private String senderId;
    private String receiverId;
    private String contentId;
    private int amount;
    private String description;
    private int type;
    private Date dateOccured = new Date();
    private boolean approved = true;
    private boolean rejected = false;

    private User sender;
    private User receiver;
    private Content content;

    public Transaction() {
    }

    public Transaction(String senderId, String receiverId, String rewardId, int amount, String description, int type, boolean approved) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.contentId = rewardId;
        this.amount = amount;
        this.description = description;
        this.type = type;
        this.dateOccured = dateOccured;
        this.approved = approved;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getContentId() {
        return contentId;
    }

    public void setContentId(String contentId) {
        this.contentId = contentId;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Date getDateOccured() {
        return dateOccured;
    }

    public void setDateOccured(Date dateOccured) {
        this.dateOccured = dateOccured;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isRejected() {
        return rejected;
    }

    public void setRejected(boolean rejected) {
        this.rejected = rejected;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }
}
