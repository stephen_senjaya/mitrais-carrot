package com.mitrais.carrot.config;

import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

public class Utility {

//    public static String getRole

//	@Autowired
//	private Configuration freemarkerConfig;

    public static void sendEmail(JavaMailSender javaMailSender, String receiverMail, String subject, String message) throws MessagingException {

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);

//        try {
            helper.setTo(receiverMail);
            helper.setSubject(subject);

//			Map<String, Object> model = new HashMap();
//			model.put("user", "qpt");

            helper.setText(message);

//            System.out.println("to: " + receiverMail);
//            System.out.println("message: " + message);

            javaMailSender.send(mimeMessage);
//        } catch (MessagingException e) {
//            e.printStackTrace();
//        }
    }

}
