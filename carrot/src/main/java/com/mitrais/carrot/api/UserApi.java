package com.mitrais.carrot.api;

import java.util.*;

import com.mitrais.carrot.config.Constanta;
import com.mitrais.carrot.config.Utility;
import com.mitrais.carrot.dal.*;
import com.mitrais.carrot.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;

/**
 * @author Fidelis
 * @since
 * @see MessagingException
 */
//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/api/user"})
public class UserApi {

	@Autowired
	private UserDal userDal;
	@Autowired
	private TransactionDal transactionDal;
	@Autowired
	private GroupDal groupDal;
	@Autowired
	private ManagerGroupDal managerGroupDal;
	@Autowired
	private NotificationDal notificationDal;
	@Autowired
	private JavaMailSender javaMailSender;

	@GetMapping(value = "")
	public List<User> getAll() {
		List<User> listUser = userDal.findAll();
		
//		for(User u : listUser)
//			System.out.println(u.getName());

		return listUser;
	}

	/**
	 *
	 * @param user
	 * @return
	 */

	@PostMapping(value = "")
	public User addEditUser(@Valid @RequestBody User user) {
		user.setSupervisor(null);
		user.setGroup(null);
		user.setToken(null);
		user.setCarrotOfTheMonth(null);
		user.setCarrotOfTheYear(null);
		user.setCarrotToOther(null);
		user.setCarrotToSocial(null);
		user.setCarrotToReward(null);
		String lowerCaseEmail = user.getEmail().toLowerCase();
		user.setEmail(lowerCaseEmail);

		User oldUser = null;
		User userByEmail = null;

		if (user.getId() != null && !user.getId().isEmpty()){
			oldUser = userDal.getUserById(user.getId());
			userByEmail = userDal.getUserByEmail(lowerCaseEmail);

			if (userByEmail != null && !oldUser.getId().equals(userByEmail.getId()))
				return null;
		} else if (userByEmail != null){
			return null;
		}

		User resUser = userDal.addEditUser(user);

		if (resUser != null && oldUser != null){
			if (!oldUser.getPassword().equals(resUser.getPassword())){
				try {
					Utility.sendEmail(javaMailSender, resUser.getEmail(), "Change password", "Your password successfully changed!");
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}
		}

		return resUser;
	}

	@GetMapping(value = "/{userId}")
	public User getUser(@PathVariable String userId) {
		return userDal.getUserById(userId);
	}

	@GetMapping(value = "/email/{email}")
	public User getUserByEmail(@PathVariable String email) {
		return userDal.getUserByEmail(email);
	}

	@GetMapping(value = {"/birthday", "/birthday/{days}"})
	public List<User> getBirthdayUsers(@PathVariable Optional<Integer> days) {
		int day = 0;

		if (days.isPresent())
			day = days.get();

		List<User> users = new ArrayList<>();

		Date date = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.DATE, day);

		date = calendar.getTime();

//		users = userDal.getByBirthdate(date);


		List<Notification> notifications = notificationDal.getByTypeAndDate(Constanta.BIRTHDAY_NOTIF, date);
		for (Notification notification: notifications){
			if (notification.getUserId() != null) {
				User user = userDal.getUserById(notification.getUserId());
				users.add(user);
			}
		}

		return users;
	}

	@GetMapping(value = "/senior-manager")
	public List<User> getSeniorManagers() {
		return userDal.getSeniorManagers();
	}

	@GetMapping(value = "/manager")
	public List<User> getManagers() {
		return userDal.getManagers();
	}

	@GetMapping(value = "/employee")
	public List<User> getEmployees() {
		return userDal.getEmployees();
	}


	@GetMapping(value = "/plus")
	public List<User> getUsersPlus() {
		List<User> users = userDal.findAll();
		for (User user: users)
			setUserPlus(user);
		return users;
	}

	@PostMapping(value = "/plus")
	public User getUserPlus(@RequestBody User user) {
		setUserPlus(user);

		return user;
	}
	@GetMapping(value = "/employee-plus")
	public List<User> getEmployeesPlus() {
		List<User> users = userDal.getEmployees();
		for (User user: users)
			setUserPlus(user);
		return users;
	}

	@GetMapping(value = "/ungroup-employee")
	public List<User> getUngroupEmployees() {
		return userDal.getUngroupEmployees();
	}

	@GetMapping(value = "/ungroup-manager")
	public List<User> getUngroupManagers() {
		return userDal.getUngroupManagers();
	}

	@GetMapping(value = "/supervisor/{supervisorId}")
	public List<User> getUsersBySupervisor(@PathVariable String supervisorId) {
		List<User> listUser = userDal.getUsersBySupervisor(supervisorId);

		return listUser;
	}

	@GetMapping(value = "/settings/{userId}")
	public Object getAllUserSettings(@PathVariable String userId) {
		User user = userDal.getUserById(userId);
		if (user != null) {
			return userDal.getAllUserSettings(userId);
		} else {
			return "User not found.";
		}
	}
//	@GetMapping(value = "/settings/{userId}/{key}")
//	public String getUserSetting(@PathVariable String userId, @PathVariable String key) {
//		return userDal.getUserSetting(userId, key);
//	}
//
//	@GetMapping(value = "/settings/{userId}/{key}/{value}")
//	public String addUserSetting(@PathVariable String userId, @PathVariable String key, @PathVariable String value) {
//		User user = userDal.getUserById(userId);
//		if (user != null) {
//			user.getSettings().put(key, value);
//			userDal.addEditUser(user);
//			return "Key added";
//		} else {
//			return "User not found.";
//		}
//	}

	public void setUserPlus(User user){
		Date dateNow = new Date();

		int carrotOfTheMonth = 0;
		int carrotOfTheYear = 0;
		int carrotToReward = 0;
		int carrotToSocial = 0;
		int carrotToOther = 0;

		List<Transaction> transactions = transactionDal.getTransactionsByUser(user.getId());
		for (Transaction transaction: transactions){
//				System.out.println(transaction.getType());
			switch (transaction.getType()){
				case Constanta.ACHIEVEMENT_TYPE:
//						System.out.println("ACHIEVEMENT_TYPE");
					if (dateNow.getYear() == transaction.getDateOccured().getYear()){
						carrotOfTheYear += transaction.getAmount();
						if (dateNow.getMonth() == transaction.getDateOccured().getMonth())
							carrotOfTheMonth += transaction.getAmount();
					}
					break;
				case Constanta.REWARD_TYPE:
//						System.out.println("REWARD_TYPE");
					if (transaction.getSenderId() != null && transaction.getSenderId().equals(user.getId()))
						carrotToReward += transaction.getAmount();
					else
						carrotToReward -= transaction.getAmount();
					break;
				case Constanta.SOCIAL_TYPE:
//						System.out.println("SOCIAL_TYPE");
					carrotToSocial += transaction.getAmount();
					break;
				case Constanta.SHARING_TYPE:
//						System.out.println("SHARING_TYPE");
//						System.out.println(transaction.getSenderId());
//						System.out.println(user.getId());
					if (transaction.getSenderId().equals(user.getId()))
						carrotToOther += transaction.getAmount();
					else {
						if (dateNow.getYear() == transaction.getDateOccured().getYear()){
							carrotOfTheYear += transaction.getAmount();
							if (dateNow.getMonth() == transaction.getDateOccured().getMonth())
								carrotOfTheMonth += transaction.getAmount();
						}
					}
					break;
			}
		}

		user.setCarrotOfTheMonth(carrotOfTheMonth);
		user.setCarrotOfTheYear(carrotOfTheYear);
		user.setCarrotToReward(carrotToReward);
		user.setCarrotToSocial(carrotToSocial);
		user.setCarrotToOther(carrotToOther);


		if (user.getGroupId() != null && !user.getGroupId().isEmpty()) {
			if (user.getRole() == Constanta.EMPLOYEE_ROLE){
				Group group = groupDal.getGroupById(user.getGroupId());
				if (group.getManagerId() != null && !group.getManagerId().isEmpty()) {
					User manager = userDal.getUserById(group.getManagerId());
					group.setManager(manager);
				}
				user.setGroup(group);
			}
		}

	}

}
