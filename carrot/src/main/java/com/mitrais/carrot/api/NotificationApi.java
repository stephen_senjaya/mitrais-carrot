package com.mitrais.carrot.api;

import com.mitrais.carrot.dal.NotificationDal;
import com.mitrais.carrot.model.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/api/notification"})
public class NotificationApi {

	@Autowired
	private NotificationDal notificationDal;

	@GetMapping(value = "")
	public List<Notification> getAll() {
		List<Notification> listNotification = notificationDal.findAll();

		return listNotification;
	}

	@PostMapping(value = "")
	public Notification addEditNotification(@RequestBody Notification notification) {
		return notificationDal.addEdit(notification);
	}

	@GetMapping(value = "user/{userId}")
	public List<Notification> getNotificationByUser(@PathVariable String userId) {
		List<Notification> listNotification = notificationDal.getByUser(userId);

		return listNotification;
	}

}
