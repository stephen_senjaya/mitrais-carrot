package com.mitrais.carrot.api;

import com.mitrais.carrot.dal.GroupDal;
import com.mitrais.carrot.dal.UserDal;
import com.mitrais.carrot.model.Group;
import com.mitrais.carrot.model.Session;
import com.mitrais.carrot.model.User;
import com.mitrais.carrot.repository.SessionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;


//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/api/session"})
public class SessionApi {

    @Autowired
    private UserDal userDal;
    @Autowired
    private GroupDal groupDal;
    @Autowired
    private SessionRepository sessionRepository;

    @PostMapping(value = "/login")
    public User login(@RequestBody User loggedInUser) {
        User user = userDal.getUserByEmail(loggedInUser.getEmail());
        if (user != null){
            if (loggedInUser.getPassword().equals(user.getPassword())) {
                if (user.getSupervisorId() != null && !user.getSupervisorId().isEmpty()) {
                    User supervisor = userDal.getUserById(user.getSupervisorId());
                    user.setSupervisor(supervisor);
                }

                if (user.getGroupId() != null && !user.getGroupId().isEmpty()) {
                    Group group = groupDal.getGroupById(user.getGroupId());
                    user.setGroup(group);
                }

                Session session = new Session();
                session.setUserId(user.getId());

                session = sessionRepository.save(session);

                user.setToken(session.getId());

                return user;
            }
        }
        return null;
    }

    @PostMapping(value = "/token")
    public User login(@RequestParam String token) {
        Optional<Session> session = sessionRepository.findById(token);

        if (session.isPresent()){
            User user = userDal.getUserById(session.get().getUserId());
            if (user != null){
                if (user.getSupervisorId() != null && !user.getSupervisorId().isEmpty()) {
                    User supervisor = userDal.getUserById(user.getSupervisorId());
                    user.setSupervisor(supervisor);
                }

                if (user.getGroupId() != null && !user.getGroupId().isEmpty()) {
                    Group group = groupDal.getGroupById(user.getGroupId());
                    user.setGroup(group);
                }

                return user;
            }
        }
        return null;
    }
}
