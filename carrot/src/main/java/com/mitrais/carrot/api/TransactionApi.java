package com.mitrais.carrot.api;

import com.mitrais.carrot.config.Constanta;
import com.mitrais.carrot.config.Utility;
import com.mitrais.carrot.dal.ContentDal;
import com.mitrais.carrot.dal.NotificationDal;
import com.mitrais.carrot.dal.TransactionDal;
import com.mitrais.carrot.dal.UserDal;
import com.mitrais.carrot.model.*;
import com.mitrais.carrot.repository.ConfigRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


//@CrossOrigin(origins = "http://localhost:4200")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping({"/api/transaction"})
public class TransactionApi {

	@Autowired
	private TransactionDal transactionDal;
	@Autowired
	private UserDal userDal;
	@Autowired
	private ContentDal contentDal;
	@Autowired
	private NotificationDal notificationDal;
	@Autowired
	private JavaMailSender javaMailSender;
	@Autowired
	private ConfigRepository configRepository;


	@GetMapping(value = "")
	public List<Transaction> getAll() {
		List<Transaction> listTransaction = transactionDal.findAll();

		for (Transaction transaction: listTransaction)
			setSenderReceiver(transaction);

		return listTransaction;
	}

	@PostMapping(value = "")
	public Transaction addEditTransaction(@RequestBody Transaction transaction) {
		if (transaction.getAmount() < 1)
			return null;

		User sender = null;
		User receiver = null;
		Content content = null;

		List<User> listAdmin = null;

		Config config = configRepository.findAll().get(0);

		transaction.setSender(null);
		transaction.setReceiver(null);
		transaction.setContent(null);
		if (transaction.getDescription() == null)
			transaction.setDescription("-");

		if (transaction.getType() == Constanta.REWARD_TYPE)
			transaction.setApproved(false);

		if (transaction.getSenderId() != null){

			sender = userDal.getUserById(transaction.getSenderId());
			User resSender = sender;

			if (!sender.isAdmin() && transaction.getReceiverId() != null){
				if (transactionDal.haveSendToday(transaction.getSenderId(), transaction.getReceiverId())){
					User rec = userDal.getUserById(transaction.getReceiverId());
					try {
						Utility.sendEmail(javaMailSender, sender.getEmail(), "Carrot sent failed",
								"Sent carrot to \"" + rec.getName() + "\" failed, you can't sent carrot to the same person more than once per day!");
					} catch (MessagingException e) {
						e.printStackTrace();
					}
					return null;
				}
			}

			if (sender.getRole() != Constanta.ROOT_ADMIN_ROLE && sender.getCarrot() - transaction.getAmount() < 0)
				return null;

			resSender.setCarrot(sender.getCarrot() - transaction.getAmount());
			resSender.setSupervisor(null);
			resSender.setGroup(null);
			resSender.setToken(null);
			resSender.setCarrotOfTheMonth(null);
			resSender.setCarrotOfTheYear(null);
			resSender.setCarrotToOther(null);
			resSender.setCarrotToSocial(null);
			resSender.setCarrotToReward(null);

			resSender = userDal.addEditUser(resSender);

			if (resSender == null)
				return null;

			if (resSender.getRole() == Constanta.MANAGER_ROLE && resSender.getCarrot() < (config != null ? config.getWarningCarrot() : 100)) {
				try {
					Utility.sendEmail(javaMailSender, resSender.getEmail(), "Stock carrot warning", "Your carrot is low, please use it wisely :)");
				} catch (MessagingException e) {
					e.printStackTrace();
				}
				notificationDal.addEdit(new Notification(Constanta.ADMIN_NOTIF, resSender.getId(), "Stock carrot warning", "Your carrot is low, please use it wisely :)"));
			}
		}

		if (transaction.getReceiverId() != null){
			receiver = userDal.getUserById(transaction.getReceiverId());
			User resReceiver = receiver;

			resReceiver.setCarrot(receiver.getCarrot() + transaction.getAmount());
			resReceiver.setSupervisor(null);
			resReceiver.setGroup(null);
			resReceiver.setToken(null);
			resReceiver.setCarrotOfTheMonth(null);
			resReceiver.setCarrotOfTheYear(null);
			resReceiver.setCarrotToOther(null);
			resReceiver.setCarrotToSocial(null);
			resReceiver.setCarrotToReward(null);

			resReceiver = userDal.addEditUser(resReceiver);
			if (resReceiver == null){
				if (sender != null)
					userDal.addEditUser(sender);
				return null;
			}

			String senderName = "Mitrais";
			if (sender != null){
				senderName = sender.getName();
				try {
					Utility.sendEmail(javaMailSender, sender.getEmail(), "Send carrot success", "Carrot for \"" + receiver.getName() + "\" is successfully sent!");
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}

			String subject = "Received carrot";
			String message = "You got carrot from \"" + senderName + "\" with message \"" + transaction.getDescription() + "\"!";

			try {
				Utility.sendEmail(javaMailSender, resReceiver.getEmail(), subject, message);
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}

		if (transaction.getContentId() != null){
			content = contentDal.getContentById(transaction.getContentId());

			String subject = null;
			String message = null;

			if (content.getType() == Constanta.REWARD_TYPE) {
				content.setCurrentCarrot(content.getCurrentCarrot() - 1);
//				content.setTimesRedeemed(content.getTimesRedeemed() + 1);
				transaction.setDescription("Exchange: " + content.getName());

				if (listAdmin == null)
					listAdmin = userDal.getAdmin();

				for (User admin: listAdmin){
					try {
						Utility.sendEmail(javaMailSender, admin.getEmail(), "Exchange reward",  "\"" + (sender != null ? sender.getName() : "Someone") + "\" exchange reward \"" + content.getName() + "\"!");
					} catch (MessagingException e) {
						e.printStackTrace();
					}
				}

//				out of stock
				if (content.getCurrentCarrot() < 1){
					subject = "Reward out of stock";
					message = "Reward \"" + content.getName() + "\" is out of stock!";
				}
			} else if (content.getType() == Constanta.SOCIAL_TYPE) {
				content.setCurrentCarrot(content.getCurrentCarrot() + transaction.getAmount());
				transaction.setDescription("Donate: " + content.getName());

//				reach target donation
				if (content.getCurrentCarrot() > content.getCarrot()){
					subject = "Social reach target";
					message = "Social \"" + content.getName() + "\" reached the target foundation!";
				}
			}

			Content resContent = contentDal.addEditContent(content);
			if (resContent == null){
				if (sender != null)
					userDal.addEditUser(sender);
				if (receiver != null)
					userDal.addEditUser(receiver);
				return null;
			}

			if (content.getType() == Constanta.ACHIEVEMENT_TYPE && receiver != null) {
				try {
					Utility.sendEmail(javaMailSender, receiver.getEmail(), "Achievement", "You achieved \"" + content.getName() + "\"!");
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			} else if (content.getType() == Constanta.SOCIAL_TYPE && sender != null) {
				try {
					Utility.sendEmail(javaMailSender, sender.getEmail(), "Donate success", "Donation for \"" + content.getName() + "\" is successfully sent!");
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}

			if (subject != null & message != null){
				if (listAdmin == null)
					listAdmin = userDal.getAdmin();

				for (User admin: listAdmin){
					try {
						Utility.sendEmail(javaMailSender, admin.getEmail(), subject, message);
					} catch (MessagingException e) {
						e.printStackTrace();
					}
				}
			}
		}

		return transactionDal.addEdit(transaction);
	}

	@PostMapping(value = "/approve")
	public Transaction approveTransaction(@RequestBody Transaction transaction) {
		Transaction result = transaction;

		result.setSender(null);
		result.setReceiver(null);
		result.setContent(null);
		result.setApproved(true);

		System.out.println(result.isRejected());

		if (result.isRejected())
			result.setDescription(transaction.getDescription() + " (Done)");
		else
			result.setDescription(transaction.getDescription() + " (Approved)");

		result = transactionDal.addEdit(result);

		if (result == null)
			return null;

		User receiver = null;
		Content content = null;

		if (result.isRejected()) {
			result.setId(null);

			String receiverId = result.getSenderId();
			result.setReceiverId(receiverId);
			result.setSenderId(null);

			if (result.getReceiverId() != null) {
				receiver = userDal.getUserById(transaction.getReceiverId());
				receiver.setCarrot(receiver.getCarrot() + transaction.getAmount());
				receiver.setSupervisor(null);
				receiver.setGroup(null);
				receiver.setToken(null);
				receiver.setCarrotOfTheMonth(null);
				receiver.setCarrotOfTheYear(null);
				receiver.setCarrotToOther(null);
				receiver.setCarrotToSocial(null);
				receiver.setCarrotToReward(null);

				receiver = userDal.addEditUser(receiver);
				if (receiver == null){
					transactionDal.addEdit(transaction);
					return null;
				}
			}
			if (result.getContentId() != null) {
				content = contentDal.getContentById(result.getContentId());
				content.setCurrentCarrot(content.getCurrentCarrot() + 1);

				content = contentDal.addEditContent(content);
				if (content == null){
					transactionDal.addEdit(transaction);
					if (receiver != null)
						userDal.addEditUser(receiver);
					return null;
				}
//				transaction.setDescription("Rejected: " + content.getName());
				result.setDescription("Exchange: " + content.getName() + " (Rejected)");
			}

			result = transactionDal.addEdit(result);
			if (result == null){
				transactionDal.addEdit(transaction);
				if (receiver != null)
					userDal.addEditUser(receiver);
				if (content != null)
					contentDal.addEditContent(content);
				return null;
			} else if (receiver != null) {
				try {
					Utility.sendEmail(javaMailSender, receiver.getEmail(), "Exchange success", "Exchange for \"" + content.getName() + "\" is success!");
				} catch (MessagingException e) {
					e.printStackTrace();
				}
			}
		} else if (receiver != null) {
			try {
				Utility.sendEmail(javaMailSender, receiver.getEmail(), "Exchange failed", "Exchange for \"" + content.getName() + "\" is success!");
			} catch (MessagingException e) {
				e.printStackTrace();
			}
		}

		return result;
	}

	@GetMapping(value = "/{transactionId}")
	public Transaction getTransaction(@PathVariable String transactionId) {
		Transaction transaction = transactionDal.getTransactionById(transactionId);

		setSenderReceiver(transaction);

		return transaction;
	}

	@GetMapping(value = "/user/{userId}")
	public List<Transaction> getTransactionsByUser(@PathVariable String userId) {
		List<Transaction> listTransaction = transactionDal.getTransactionsByUser(userId);

		for (Transaction transaction: listTransaction)
			setSenderReceiver(transaction);

		return listTransaction;
	}

	@GetMapping(value = "/{userId}/{type}")
	public List<Transaction> getTransactions(@PathVariable String userId, @PathVariable int type) {
		List<Transaction> listTransaction = new ArrayList<>();

		if (type == Constanta.ACHIEVEMENT_TYPE){
			List<Transaction> transactions = transactionDal.getTransactionsByUser(userId);

			listTransaction = transactions.stream()
					.filter(transaction -> (transaction.getType() == Constanta.ACHIEVEMENT_TYPE || transaction.getSenderId() != userId))
					.collect(Collectors.toList());
		}else if (type == Constanta.SHARING_TYPE){
			List<Transaction> transactions = transactionDal.getTransactionsByUser(userId);

			listTransaction = transactions.stream()
					.filter(transaction -> (transaction.getType() == Constanta.SHARING_TYPE || transaction.getSenderId() == userId))
					.collect(Collectors.toList());
		} else {
			listTransaction = transactionDal.getTransactions(userId, type);
		}


		for (Transaction transaction: listTransaction)
			setSenderReceiver(transaction);

		return listTransaction;
	}

	@GetMapping(value = "/type/{type}")
	public List<Transaction> getTransactionByType(@PathVariable int type) {
		List<Transaction> listTransaction = transactionDal.getTransactionByType(type);

		for (Transaction transaction: listTransaction)
			setSenderReceiver(transaction);

		return listTransaction;
	}

	@GetMapping(value = "/sender/{senderId}")
	public List<Transaction> getTransactionsBySender(@PathVariable String senderId) {
		List<Transaction> listTransaction = transactionDal.getTransactionsBySender(senderId);

		for (Transaction transaction: listTransaction)
			setSenderReceiver(transaction);

		return listTransaction;
	}

	@GetMapping(value = "/receiver/{receiverId}")
	public List<Transaction> getTransactionsByReceiver(@PathVariable String receiverId) {
		List<Transaction> listTransaction = transactionDal.getTransactionsByReceiver(receiverId);

		for (Transaction transaction: listTransaction)
			setSenderReceiver(transaction);

		return listTransaction;
	}

	public void setSenderReceiver(Transaction transaction){

		if (transaction.getSenderId() != null && !transaction.getSenderId().isEmpty()){
			User sender = userDal.getUserById(transaction.getSenderId());
			transaction.setSender(sender);
		} else {
			transaction.setSender(getTransacionUser(transaction));
		}

		if (transaction.getReceiverId() != null && !transaction.getReceiverId().isEmpty()) {
			User receiver = userDal.getUserById(transaction.getReceiverId());
			transaction.setReceiver(receiver);
		} else {
			transaction.setReceiver(getTransacionUser(transaction));
		}

		if (transaction.getContentId() != null && !transaction.getContentId().isEmpty()) {
			Content content = contentDal.getContentById(transaction.getContentId());
			transaction.setContent(content);
		}

	}

	public User getTransacionUser(Transaction transaction){
		User user = new User();
		switch (transaction.getType()) {
			case Constanta.ACHIEVEMENT_TYPE:
				user.setName("Mitrais");
				break;
			case Constanta.REWARD_TYPE:
				user.setName("Bazaar");
				break;
			case Constanta.SOCIAL_TYPE:
				user.setName("Social");
				break;

			default:
				user.setName("Mitrais");
				break;
		}

		return user;
	}

//	public void sendEmail(String receiverMail, String subject, String message){
//		MimeMessage mimeMessage = javaMailSender.createMimeMessage();
//		MimeMessageHelper helper = new MimeMessageHelper(mimeMessage);
//
//		try {
//			helper.setTo(receiverMail);
//			helper.setSubject(subject);
//
////			Map<String, Object> model = new HashMap();
////			model.put("user", "qpt");
//
//			helper.setText(message);
//
//			javaMailSender.send(mimeMessage);
//		} catch (MessagingException e) {
//			e.printStackTrace();
//		}
//	}

}
