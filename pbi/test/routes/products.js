var express = require('express');
var router = express.Router();
const mongoose = require('mongoose');

const Product = require('../models/product-model');
const ProductsRepository = require('../repositories/products-repository');

const MONGODB_URL = "mongodb+srv://user:user@training-t4zdk.mongodb.net/carrot-db";


router.get('/', function(req, res, next) {
  const productsRepository = new ProductsRepository(MONGODB_URL);

  productsRepository.getAll(null, (err, result) => {
    if (err) {
      return res.status(err.status).send(err.message);
    }
    return res.status(200).send(result);
  })

});

router.get('/:id', function(req, res, next) {
  const id = req.params.id;
  const productsRepository = new ProductsRepository(MONGODB_URL);

  productsRepository.get(id, (err, result) => {
    if (err) {
      return res.status(err.status).send(err.message);
    }
    return res.status(200).send(result);
  })

});

router.delete('/delete/:id', function(req, res, next) {
  const id = req.params.id;
  const productsRepository = new ProductsRepository(MONGODB_URL);

  productsRepository.delete(id, (err, result) => {
    if (err) {
      return res.status(err.status).send(err.message);
    }
    return res.status(200).send(result);
  })

});

router.put('/update/:id', function(req, res, next) {
  const id = req.params.id;
  const changedProduct = req.body;
  const productsRepository = new ProductsRepository(MONGODB_URL);

  productsRepository.update(id, changedProduct, (err, result) => {
    if (err) {
      return res.status(err.status).send(err.message);
    }
    return res.status(200).send(result);
  })

});

router.post('/create', function(req, res, next) {
  // Get submitted JSON Payload
  const productsRepository = new ProductsRepository(MONGODB_URL);
  const jsonPayload = req.body;

  productsRepository.createProduct(jsonPayload, (err, result) => {
    if (err) {
      return res.status(err.status).send(err.message);
    }
    return res.status(200).send(result);
  })

});


module.exports = router;

