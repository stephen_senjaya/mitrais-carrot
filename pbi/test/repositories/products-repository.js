
'use strict';
const mongoose = require('mongoose');
const Product = require('../models/product-model');

class ProductsRepository{
  constructor(mongodbUrl){
    this.mongodbUrl = mongodbUrl;
  }
  
  getAll(filter, callback) {

    if (!filter)
      filter = {};

    console.log(filter);

    mongoose.connect(this.mongodbUrl);

    mongoose.connection.on('error', (err) => {
      console.log(err);
      return callback({
        status: 500,
        message: err
      });
    });

    Product.find(filter, (err, result) => {

      mongoose.disconnect();

      if(err){
        console.log(err);
        return callback({
          status: 500,
          message: err
        });
      }

      return callback(null, result);

    })
  }

  get(id, callback) {

    mongoose.connect(this.mongodbUrl);

    mongoose.connection.on('error', (err) => {
      console.log(err);
      return callback({
        status: 500,
        message: err
      });
    });

    Product.findById(id, (err, result) => {

      mongoose.disconnect();

      if(err){
        console.log(err);
        return callback({
          status: 500,
          message: err
        });
      }

      return callback(null, result);

    })
  }

  delete(id, callback) {

    mongoose.connect(this.mongodbUrl);

    mongoose.connection.on('error', (err) => {
      console.log(err);
      return callback({
        status: 500,
        message: err
      });
    });

    Product.findByIdAndRemove(id, (err, result) => {

      mongoose.disconnect();

      if(err){
        console.log(err);
        return callback({
          status: 500,
          message: err
        });
      }

      return callback(null, result);

    })
  }

  update(id, product, callback) {

    const changedProduct = new Product(product);
    
    const errValidation = changedProduct.validateSync();

    if (errValidation) {
      console.log(errValidation);
      return callback({
        status: 400,
        message: errValidation
      });
    }

    mongoose.connect(this.mongodbUrl);

    mongoose.connection.on('error', (err) => {
      console.log(err);
      return callback({
        status: 500,
        message: err
      });
    });

    Product.findByIdAndUpdate(id, product, (err, result) => {

      mongoose.disconnect();

      if(err){
        console.log(err);
        return callback({
          status: 500,
          message: err
        });
      }

      return callback(null, result);

    })
  }

  createProduct(jsonPayload, callback) {
    const newProduct = new Product(jsonPayload);
    
    const errValidation = newProduct.validateSync();

    if (errValidation) {
      console.log(errValidation);
      return callback({
        status: 400,
        message: errValidation
      });
    }

    mongoose.connect(this.mongodbUrl);

    mongoose.connection.on('error', (err) => {
      console.log(err);
      return callback({
        status: 500,
        message: err
      });
    });

    newProduct.save((err, result) => {

      mongoose.disconnect();

      if(err){
        console.log(err);
        return callback({
          status: 500,
          message: err
        });
      }

      return callback(null, result);

    })
  }

}

module.exports = ProductsRepository;