const mongoose = require('mongoose');

const MODEL_NAME = 'Product';

// Define Product Schema
const ProductSchema = mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  price: {
    amount: {
      type: Number,
      required: true
    },
    currency: {
      type: String,
      required: true
    }
  }
})

// TODO: Define Product Model
const Product = mongoose.model(MODEL_NAME, ProductSchema);

// TODO: Export the Product Model
module.exports = Product;
