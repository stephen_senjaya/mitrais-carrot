import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        try {
//            MongoClientURI uri = new MongoClientURI("mongodb+srv://user:user@training-t4zdk.mongodb.net/carrot-db");
            MongoClient mongoClient = new MongoClient("localhost");

            List<String> databases = mongoClient.getDatabaseNames();

            for (String dbName : databases) {
                System.out.println("- Database: " + dbName);

                DB db = mongoClient.getDB(dbName);

                Set<String> collections = db.getCollectionNames();
                for (String colName : collections) {
                    System.out.println("\t + Collection: " + colName);
                }
            }

            mongoClient.close();
        } catch (UnknownHostException e) {
            e.printStackTrace();
        }
    }

}
