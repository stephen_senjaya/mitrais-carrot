// debugger

var owner = ["Owner 1", "Owner 2", "Owner 3", "Manager 1", "Manager 2", "Manager 3", "Ownager 1", "Ownager 2", "Ownager 3"];

function checkWork()
{
    console.log(document.getElementById("barnName").value)
    if(document.getElementById("barnName").value != "" ||
       document.getElementById("carrotEmployee").value != "" ||
       document.getElementById("barnOwner").value != "")
       {
           var back = document.getElementById("backButton")
            back.setAttribute("data-toggle",  "modal");
            back.setAttribute("data-target",  "#exampleModalCenter");
       }
    else
       window.location.href='index-farmer.html';
}

function dropdownOwner(inp, arr) 
{
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) 
    {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();

        if (!val) { return false;}
            currentFocus = -1;

        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");

        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);

        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) 
        {
            /*check if the item starts with the same letters as the text field value:*/
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) 
            {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");

                /*make the matching letters bold:*/
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);

                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";

                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) 
                {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });

                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
          /*If the arrow DOWN key is pressed,
          increase the currentFocus variable:*/
          currentFocus++;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 38) { //up
          /*If the arrow UP key is pressed,
          decrease the currentFocus variable:*/
          currentFocus--;
          /*and and make the current item more visible:*/
          addActive(x);
        } else if (e.keyCode == 13) {
          /*If the ENTER key is pressed, prevent the form from being submitted,*/
          e.preventDefault();
          if (currentFocus > -1) {
            /*and simulate a click on the "active" item:*/
            if (x) x[currentFocus].click();
          }
        }
    });
    function addActive(x) {
      /*a function to classify an item as "active":*/
      if (!x) return false;
      /*start by removing the "active" class on all items:*/
      removeActive(x);
      if (currentFocus >= x.length) currentFocus = 0;
      if (currentFocus < 0) currentFocus = (x.length - 1);
      /*add class "autocomplete-active":*/
      x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
      /*a function to remove the "active" class from all autocomplete items:*/
      for (var i = 0; i < x.length; i++) {
        x[i].classList.remove("autocomplete-active");
      }
    }

    function closeAllLists(elmnt) 
    {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) 
        {
            if (elmnt != x[i] && elmnt != inp) 
            {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) 
    {
      closeAllLists(e.target);
    });
}

dropdownOwner(document.getElementById("barnOwner"), owner);

function createBarn()
{
    if(document.getElementById("barnName").value == "" ||
       document.getElementById("carrotEmployee").value == "" ||
       document.getElementById("barnOwner").value == "")
    {
        alert("Please fill all field.")
    }
    else if(isNaN(document.getElementById("carrotEmployee").value) != false)
    {
        alert("Carrot per Employee must be number.")
    }
    else if( $.inArray(document.getElementById("barnOwner").value, ["Owner 1", "Owner 2", "Owner 3", "Manager 1", "Manager 2", "Manager 3", "Ownager 1", "Ownager 2", "Ownager 3"]) < 0 )
    {      
        alert("Please fill the right Manager.")
    }
    else
    {
        var name = document.getElementById("barnName").value;
        var id = "name";
        var val = name;
        localStorage.setItem(id, val);
        
        var owner = document.getElementById("barnOwner").value;
        var id = "owner";
        var val = owner;
        localStorage.setItem(id, val);
    
        var startPeriod = document.getElementById("startDate").value; //YYYY-MM-DD
        var parsedStart = moment(startPeriod).format('DD/MM/YYYY');
        var id = "start period";
        var val = parsedStart;
        localStorage.setItem(id, val);
    
        var endPeriod = document.getElementById("endDate").value;
        var parsedEnd = moment(endPeriod).format('DD/MM/YYYY');
        var id = "end period";
        var val = parsedEnd;
        localStorage.setItem(id, val);
    
        var carrotEmployee = document.getElementById("carrotEmployee").value;
        var id = "carrot employee";
        var val = carrotEmployee;
        localStorage.setItem(id, val);
    
        var totalEmployee = document.getElementById("totalEmployee").value;
        var id = "total employee";
        var val = totalEmployee;
        localStorage.setItem(id, val);
    
        $("#createBarn").attr("href", "index-farmer.html");
<<<<<<< Updated upstream
        window.location = ("/html/index-farmer.html");
=======
>>>>>>> Stashed changes
    }
}