
var data = [];
var dataTable = null;

$(document).ready(function(){
    $.ajax({url: "/data.json", success: function(res){
        data = res;
        setTable(true);
        // _.forEach(res, function(value) {
        //     console.log(value);
        // });
    }});
});

function setTable(searchData) {
    if (searchData) {
        search();
    }
}

function search() {
    var carrotType = document.getElementById("carrot-type").value;
    var transaction = '';

    var i = 1;
    _.forEach(data, function(trx) {
        // console.log(trx);
        if (carrotType == '') {
            transaction += 
            '<tr>' +
                '<th scope="row">' + i + '</th>' +
                '<td>' + trx.type + '</td>' +
                '<td>' + trx.toFrom + '</td>' +
                '<td>' + trx.desc + '</td>' +
                '<td>' + trx.carrot + '</td>' +
                '<td>' + trx.date + '</td>' +
            '</tr>';

            i++;
        } else if (carrotType == trx.type) {
            transaction += 
            '<tr>' +
                '<th scope="row">' + i + '</th>' +
                '<td>' + trx.type + '</td>' +
                '<td>' + trx.toFrom + '</td>' +
                '<td>' + trx.desc + '</td>' +
                '<td>' + trx.carrot + '</td>' +
                '<td>' + trx.date + '</td>' +
            '</tr>';

            i++;
        }
    });
    
    if (dataTable != null) {
        // dataTable.draw();
        dataTable.clear();
        dataTable.destroy();
    }

    // document.getElementById("transaction").innerHTML = transaction;
    $("#transaction").html(transaction);

    if (carrotType == '') {
        dataTable = $('#mytable').DataTable({
            "searching": false,
            "ordering": false,
        });
    } else {
        dataTable = $('#mytable').DataTable({
            "paging":   false,
            "searching": false,
            "ordering": false,
            "info":     false
        });
    }
}