import { Component, OnInit } from '@angular/core';

import { User } from '../../model/user';
import { Content } from '../../model/content';
import { UserService } from '../../service/user.service';
import { TransactionService } from '../../service/transaction.service';
import { ContentService } from '../../service/content.service';
import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';

import { Transaction } from '../../model/transaction';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-achievement',
  templateUrl: './achievement.component.html',
  styleUrls: ['./achievement.component.css']
})
export class AchievementComponent implements OnInit {

  constructor(
    private userService: UserService,
    private contentService: ContentService
  ) { }

  user: User;
  contents: Content[];
  selectedContent: Content;

  ngOnInit() {
    this.user = this.userService.getLoggedInUser();
    if (this.user) {
      this.contentService.fetchContentPlus(Constanta.ACHIEVEMENT_TYPE, this.user.id)
      .subscribe(res => {
        console.log(res);
        this.contents = res;
      });
    }
    
  }

  onSelect(content: Content){
    this.selectedContent = content;
  }

}
