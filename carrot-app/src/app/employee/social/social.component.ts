import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { User } from '../../model/user';
import { Content } from '../../model/content';
import { UserService } from '../../service/user.service';
import { TransactionService } from '../../service/transaction.service';
import { ContentService } from '../../service/content.service';
import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';

import { Transaction } from '../../model/transaction';
import { AppComponent } from '../../app.component';

@Component({
  selector: 'app-social',
  templateUrl: './social.component.html',
  styleUrls: ['./social.component.css']
})
export class SocialComponent implements OnInit {
  @ViewChild('closeBtn') closeBtn: ElementRef;

  constructor(
    private transactionService: TransactionService,
    private userService: UserService,
    private contentService: ContentService,
    private appComponent: AppComponent,
  ) { }

  user: User;
  contents: Content[];
  selectedContent: Content;
  amountValue: number = 1;
  isExchangeLoading = false;

  ngOnInit() {
    this.user = this.userService.getLoggedInUser();
    let groupId = this.user && this.user.groupId ? this.user.groupId : null;
    this.contentService.fetchContentsByType(Constanta.SOCIAL_TYPE, groupId)
      .subscribe(res => {
        console.log(res);
        // this.contents = res.filter(content => {
        //   return content.active && !content.deleted
        // });
        this.contents = res;
      });
    
  }

  onSelect(content: Content){
    if (!this.user) {
      this.user = this.userService.getLoggedInUser();
    }
    this.selectedContent = content;
  }

  exchange(content: Content){
    if (content.carrot > this.user.carrot){
      this.appComponent.showError('Not enough carrot!');
      return;
    }
    
    this.isExchangeLoading = true;

    let transaction = new Transaction();
    transaction.type = Constanta.SOCIAL_TYPE;
    transaction.senderId = this.user.id;
    transaction.contentId = content.id;
    transaction.amount = this.amountValue;
    transaction.description = 'Social: ' + content.name;


    this.transactionService.postTransaction(transaction)
      .subscribe(res => {
        console.log(res);
        this.isExchangeLoading = false;
        this.closeModal();

        if (res) {
          this.user.carrot -= transaction.amount;
          content.currentCarrot -= 1;
          this.appComponent.showSuccess('Donate Success!');
        } else {
          this.appComponent.showError('Donate Failed!');
        }
      });
  }

  changeAmount(amount: number){
    console.log(amount);
    this.amountValue = amount;
    if (this.amountValue > this.user.carrot) {
      this.amountValue = this.user.carrot;
    }
    console.log(this.amountValue);
  }

  closeModal(){
    this.closeBtn.nativeElement.click();
  }

}
