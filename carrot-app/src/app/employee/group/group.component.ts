import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { User } from '../../model/user';
import { UserService } from '../../service/user.service';
import { TransactionService } from '../../service/transaction.service';
import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';

import { Transaction } from '../../model/transaction';
import { AppComponent } from '../../app.component';
import { Config } from '../../model/config';
import { ConfigService } from '../../service/config.service';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  @ViewChild('closeBtn') closeBtn: ElementRef;

  constructor(
    private userService: UserService,
    private transactionService: TransactionService,
    private configService: ConfigService,
    private appComponent: AppComponent,
  ) { }

  users: User[];
  loggedInUser: User;
  selectedUser: User;
  modalShare: boolean;
  amountValue: number = 1;
  isExchangeLoading = false;
  config: Config;

  ngOnInit() {
    this.loggedInUser = this.userService.getLoggedInUser();
    this.getUsers();
    this.config = this.configService.getConfigData();
    if (this.loggedInUser && this.loggedInUser.carrot < this.config.carrotLimit) {
      this.config.carrotLimit = this.loggedInUser.carrot;
    }
  }

  Constanta = Constanta;
  Utility = Utility;

  getUsers(){
    // this.users = this.userService.getAll();

    if (!this.users) {
      this.userService.fetchUsersBySupervisor(this.loggedInUser.supervisorId)
        .subscribe(users => {
          console.log(users);
          this.users = users.filter(user => {
            return user.id != this.loggedInUser.id;
          })
        });
    }
  }

  selectUser(user: User, modalShare: boolean){

    if (!this.loggedInUser) {
      this.loggedInUser = this.userService.getLoggedInUser();
    }

    console.log(modalShare);
    this.selectedUser = user;
    if(modalShare)
      this.modalShare = true;
    else
      this.modalShare = false;
  }

  sendCarrot(description: string){
    this.isExchangeLoading = true;
    if (!this.loggedInUser) {
      this.loggedInUser = this.userService.getLoggedInUser();
    }
    let senderId = this.loggedInUser.id;
    let receiverId = this.selectedUser.id;
    let type = Constanta.SHARED_TYPE;
    let amount = this.amountValue;

    if (amount > 0) {
      this.transactionService.postTransaction({type, senderId, receiverId, amount, description} as Transaction)
        .subscribe(res => {
          console.log(res);
          this.isExchangeLoading = false;
          this.closeModal();
          if (res) {
            this.loggedInUser.carrot -= amount;
            this.selectedUser.carrot += amount;
            this.appComponent.showSuccess('Send Success!');
          } else {
            this.appComponent.showError('Send Failed!');
          }
        });
      
    } else {
      this.appComponent.showError('Send Failed!');
    }
  }

  changeAmount(amount: number){
    this.amountValue = amount;
    if (this.amountValue > this.config.carrotLimit) {
      this.amountValue = this.config.carrotLimit;
    }
    console.log(this.amountValue);
  }

  closeModal(){
    this.closeBtn.nativeElement.click();
  }

}
