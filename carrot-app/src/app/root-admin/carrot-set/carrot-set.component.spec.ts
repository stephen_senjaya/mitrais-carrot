import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarrotSetComponent } from './carrot-set.component';

describe('CarrotSetComponent', () => {
  let component: CarrotSetComponent;
  let fixture: ComponentFixture<CarrotSetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarrotSetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarrotSetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
