import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";

import { User } from '../model/user';
import { Transaction } from '../model/transaction';
import { Config } from '../model/config';
import { Content } from '../model/content';

import * as Constanta from '../config/constanta';

import { RootAdminService } from '../service/root-admin.service';
import { AdminService } from '../service/admin.service';
import { UserService } from '../service/user.service';
import { TransactionService } from '../service/transaction.service';
import { ConfigService } from '../service/config.service';

import { AppComponent } from '../app.component'

import { Subject } from 'rxjs';

@Component({
  selector: 'app-root-admin',
  templateUrl: './root-admin.component.html',
  styleUrls: ['./root-admin.component.css']
})
export class RootAdminComponent implements OnInit {
 
  isAdmin: boolean;
  isManager: boolean;
  isStaff: boolean;
  isSocial: boolean;
  isGiveCarrot: boolean;
  isSetCarrot:boolean;

  constructor(
    private rootAdminService: RootAdminService,
    private adminService:AdminService,
    private userService:UserService,
    private transactionService:TransactionService,
    private configService:ConfigService,
    private router: Router,
    private appComponent: AppComponent
  ) { }

    
  userTemp: User = new User;
  loggedInUser:User = new User;

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();
    
    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
            } else
              this.router.navigate(['/']);
          })
      } else 
        this.router.navigate(['/']);
    }

    this.openAdminList();
  }

  openAdminList()
  {
    this.isAdmin = true;
    this.isStaff = false;
    this.isManager = false;
    this.isSocial = false;
    this.isSetCarrot = false;
    this.isGiveCarrot = false;
  }

  openManagerList()
  {
    this.isAdmin = false;
    this.isStaff = false;
    this.isManager = true;
    this.isSocial = false;
    this.isSetCarrot = false;
    this.isGiveCarrot = false;
  }

  openStaffList()
  {
    this.isAdmin = false;
    this.isStaff = true;
    this.isManager = false;
    this.isSocial = false;
    this.isSetCarrot = false;
    this.isGiveCarrot = false;
  }

  openSocial()
  {
    this.isAdmin = false;
    this.isStaff = false;
    this.isManager = false;
    this.isSocial = true;
    this.isSetCarrot = false;
    this.isGiveCarrot = false;
  }

  openSetCarrot()
  {
    this.isAdmin = false;
    this.isStaff = false;
    this.isManager = false;
    this.isSocial = false;
    this.isSetCarrot = true;
    this.isGiveCarrot = false;
  }

  openGiveCarrot()
  {
    this.isAdmin = false;
    this.isStaff = false;
    this.isManager = false;
    this.isSocial = false;
    this.isSetCarrot = false;
    this.isGiveCarrot = true;
  }
}
