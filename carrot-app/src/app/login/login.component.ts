import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";

import { User } from '../model/user';
import { UserService } from '../service/user.service';
import * as Constanta from '../config/constanta';
import { AppComponent } from '../app.component';
import { NotificationService } from '../service/notification.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(
    private userService: UserService,
    private router: Router,
    private notificationService: NotificationService,
  ) { }

  user: User;
  isLoading: boolean = true;
  isLoadingLogin: boolean = false;
  errMsg: string = null;

  ngOnInit() {
    this.user = this.userService.getLoggedInUser();
    if (this.user){
      this.move(this.user);
      this.isLoading = false;
    } else {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if(res){
              this.user = res;
              this.isLoading = false;
              this.move(this.user);
            } else
              this.isLoading = false;
          });
      } else
        this.isLoading = false;
    }
  }

  login(email: string, password: string): void {
    this.isLoadingLogin = true;
    email = email.trim();
    if (!email) { 
      this.isLoadingLogin = false;
      return; 
    }
    this.userService.login({ email, password } as User)
      .subscribe(user => {
        if(user){
          console.log(user);
          localStorage.setItem(Constanta.TOKEN_KEY_NAME, user.token);
          this.isLoadingLogin = false;
          this.move(user);
        } else {
          this.errMsg = 'user not found!';
          this.isLoadingLogin = false;
        }
      });
  }

  move(user: User){
    if (user) {
      this.userService.setLoggedInUser(user);

      // let appComponent = new AppComponent(this.notificationService, this.userService, this.router);
      // appComponent.ngOnInit();
      
      
      if(user.admin)
        this.router.navigate(['admin']);
      else {
        switch (user.role) {
          case Constanta.MANAGER_ROLE:
            this.router.navigate(['manager']);
            break;
          case Constanta.EMPLOYEE_ROLE:
            this.router.navigate(['employee']);
            break;
          case Constanta.ROOT_ADMIN_ROLE:
            this.router.navigate(['root-admin']);
            break;
          case Constanta.SENIOR_MANAGER_ROLE:
            this.router.navigate(['senior-manager']);
            break;
          
          default:
            break;
        }
      }
    }
  }

}
