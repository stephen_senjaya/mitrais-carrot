

import { DatePipe } from '@angular/common';
import * as Constanta from './constanta';

export function transformDate(date: string, format: string): string {
  if(!format)
    format = 'MMM dd, yyyy';
  let pipe = new DatePipe('en-US');
  return pipe.transform(date, format);
}

export function getDate(date: string, format: string): Date {
  if(!format)
    format = 'MMM dd, yyyy';
  return new Date(transformDate(date, format));
}


export function getType(type: number): string {
  switch (type) {
    case Constanta.ACHIEVEMENT_TYPE:
      return 'Achievement';
    case Constanta.REWARD_TYPE:
      return 'Reward';
    case Constanta.SOCIAL_TYPE:
      return 'Social';
    case Constanta.SHARED_TYPE:
      return 'Shared';
  
    default:
      return '-';
  }
}


export function getRole(type: number): string {
  switch (type) {
    case Constanta.MANAGER_ROLE:
      return 'Manager';
    case Constanta.EMPLOYEE_ROLE:
      return 'Employee';
    case Constanta.ROOT_ADMIN_ROLE:
      return 'Root Admin';
    case Constanta.SENIOR_MANAGER_ROLE:
      return 'Senior Manager';
  
    default:
      return '-';
  }
}

export function isValidEmail(email: string): boolean {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email.toLowerCase());
}