import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupStaffComponent } from './group-staff.component';

describe('GroupStaffComponent', () => {
  let component: GroupStaffComponent;
  let fixture: ComponentFixture<GroupStaffComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupStaffComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupStaffComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
