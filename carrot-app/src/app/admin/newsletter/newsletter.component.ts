import { Component, OnInit, AfterViewInit, group } from '@angular/core';
import { Router } from "@angular/router";

import { ViewChild } from '@angular/core'
import { NgModule } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';

import { Content } from '../../model/content';
import { ContentChange } from '../../model/content-change';
import { User } from '../../model/user';
import { Group } from '../../model/group';
import { Transaction } from '../../model/transaction';
import { Notification } from '../../model/notification';

import { AdminService } from '../../service/admin.service';
import { GroupService } from '../../service/group.service';
import { TransactionService } from '../../service/transaction.service';
import { NotificationService } from '../../service/notification.service';

import {AppComponent} from '../../app.component';

import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';

import { forEach } from '@angular/router/src/utils/collection';
import { OrderPipe } from 'ngx-order-pipe';
import { UserService } from '../../service/user.service';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs';

import * as $ from 'jquery';
import 'datatables.net';


@Component({
  selector: 'app-newsletter',
  templateUrl: './newsletter.component.html',
  styleUrls: ['./newsletter.component.css']
})
export class NewsletterComponent implements OnInit {
  isLoading: boolean = false;

  users : User [];
  loggedInUser: User;
  newsletters:Notification[];

  editorContent: string = "";

  loadingNewsletters: boolean=true;
  loaded:boolean = false;

  selectedNewsletter : Notification = new Notification;

  dtOptionsNewsletter: DataTables.Settings = {};
  dtTriggerNewsletter: Subject<any> = new Subject();

  isList : boolean = true;
  isCreate : boolean = false;

  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  constructor(
    private http: HttpClient,
    private adminService: AdminService,
    private groupService: GroupService,
    private userService: UserService,
    private transactionService: TransactionService,
    private notificationService: NotificationService,
    private appComponent: AppComponent,
    private location: Location,
    public datepipe: DatePipe,
    private orderPipe: OrderPipe,
    private router: Router
  ){}

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();

    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
              this.isLoading = false;
              if (!res.admin)
                this.router.navigate(['/']);
            } else
            {
              this.isLoading = false;
              this.router.navigate(['/']);
            }
          })
      } else {
        this.isLoading = false;
        this.router.navigate(['/']);
      }
    }
      else 
        this.isLoading = false;

  //   this.getRewards();
  //   this.getTransaction();
    this.getUsers();
  //   this.getUngroupedManager();
  //   this.getGroups();
    this.getNewsletter();

  //   this.disabledGroup = [];
  //   this.enabledGroup = [];
  }

//   //----------GET LIST METHOD----------

//   getRewards(): void 
//   {
//     this.adminService.getRewards().subscribe(rewards=> {
//       this.rewards = rewards;
//       this.dtTriggerContent.next();
//     });
//   }

  openList()
  {
    this.isList = true;
    this.isCreate = false;
  }

  openCreate()
  {
    this.isList = false;
    this.isCreate = true;
  }

  getUsers()
  {
    return this.adminService.getUsers().subscribe(users=> {
      this.users = users;
    });
  }

//   getUngroupedManager()
//   {
//     this.managers = [];
//     this.seniorManagers = [];
//     return this.userService.fetchUsers().subscribe(users => {
//       this.users = users;
//       this.users.forEach((user) => {
//         if(user.role == 4)
//           this.managers.push(user);

//         if(user.role == 8)
//           this.seniorManagers.push(user);
//       });
//     })
//   }

//   getGroups()
//   {
//     this.groups = [];
//     return this.groupService.getAllGroups().subscribe(groups => {
//         this.groups = groups;
//         this.groupService.getAllManagerGroups().subscribe(mGroups =>{
//           mGroups.forEach((group) =>{
//             this.groups.push(group);
//           });
//           this.loadingGroup = false;
//           this.dtTriggerGroup.next();
//         });
//       });
//   }

  getNewsletter()
  {
    this.newsletters = [];
    return this.notificationService.getAll().subscribe(notifications => {
      notifications.forEach((notification) =>{
        if(notification.type == 2)
          this.newsletters.push(notification);
      });
      this.loaded = true;
      this.loadingNewsletters = false;
      this.dtTriggerNewsletter.next();
    });
  }

//   getTransaction()
//   {
//     this.approvals = [];
//     this.transactionService.fetchTransactions().subscribe(transactions => {
//       this.transactions = transactions;
//       this.transactions.forEach((transaction) => {
//         if(transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
//           this.approvals.push(transaction);
//       });   
//       this.loadingApproval = false;
//       this.dtTriggerApproval.next();
//     });
//   }

//   //-----------------------------------

//   //----------GET TYPE METHOD----------

//   getRewardType(type : number):string
//   {
//     return Utility.getType(type);
//   }

  getDate(type: string):string
  {
    return Utility.transformDate(type, 'dd-MM-yyyy');
  }

//   getStatus(active : boolean):string
//   {
//     if(active == true)
//       return "Open";
//     else
//       return "Closed";
//   }

//   getRoleType(type : number):string
//   {
//     return Utility.getRole(type);
//   }

  getUserNameById(id:string):string
  {
    if(this.users)
    {
      let name: string = "";
      this.users.forEach(user => {
        if(user.id == id)
          name = user.name;
      });
      return name;
    }
  }

//   getContentById(id:string):string
//   {
//     if(this.rewards)
//     {
//       let name: string = "";
//       this.rewards.forEach(reward => {
//         if(reward.id == id)
//           name = reward.name;
//       });
//       return name;
//     }
//   }

//   //--------------------------------

//   //----------ADD METHOD----------
  
//   addReward(role: number, rewardName: string, rewardDesc:string, carrot: number): void 
//   {
//     if(role != null || rewardName != "" || carrot != null)
//     {
//       let rewardLimit = this.limit;
//       let expiredDate = this.expired;
  
//       if(!isNaN(carrot))
//       {
//         rewardName = rewardName.trim();
  
//         this.rewardTemp.type = 2;
//         if(!isNaN(rewardLimit) && rewardLimit && expiredDate)
//         {
//           this.rewardTemp.currentCarrot = rewardLimit;
//           this.rewardTemp.expired = expiredDate.toString();
  
//           this.rewardTemp.role = role;
//           this.rewardTemp.name = rewardName;
//           this.rewardTemp.description = rewardDesc;
    
//           this.rewardTemp.groupsId = [];
          
//           let groupsId: string[] = new Array<string>();
//           groupsId = this.rewardTemp.groupsId;
//           this.groups.forEach((group) => {
//             groupsId.push(group.id);
//           });
//           this.rewardTemp.groupsId = groupsId;
    
//           this.rewardTemp.carrot = carrot;
//           this.rewardTemp.active = true;
    
//           this.adminService.addReward(this.rewardTemp)
//             .subscribe(reward =>{
//               if(reward)
//               {
//                 this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
//                 this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
//                   dtInstance.destroy();
//                   this.rewards.push(reward);
//                   this.dtTriggerContent.next();
//                 });
//               }
//               else
//                 this.appComponent.showSuccess("Sending Failed.");
//             });
    
//         }
//         else
//           this.appComponent.showError("Redeem Limit must be number and please Fill the date and Redeem Limit")
//       }
//       else
//         this.appComponent.showError("Carrot must be a Number");     
//     }
//     else
//       this.appComponent.showError("Please fill all the field");  
//   }

//   addSocial(role: number, rewardName: string, rewardDesc:string, carrot: number): void 
//   {  
//     if(role != null || rewardName != "" || carrot != null)
//     {
//       let currentCarrot = 0;

//       if(!isNaN(carrot))
//       {
//         rewardName = rewardName.trim();

//         this.rewardTemp.type = 3;
      
//         this.rewardTemp.role = role;
//         this.rewardTemp.name = rewardName;
//         this.rewardTemp.description = rewardDesc;

//         this.rewardTemp.groupsId = [];
        
//         let groupsId: string[] = new Array<string>();
//         groupsId = this.rewardTemp.groupsId;
//         this.groups.forEach((group) => {
//           groupsId.push(group.id);
//         });
//         this.rewardTemp.groupsId = groupsId;

//         this.rewardTemp.carrot = carrot;
//         this.rewardTemp.currentCarrot = currentCarrot;
//         this.rewardTemp.active = true;

//         this.adminService.addReward(this.rewardTemp)
//           .subscribe(reward => {
//             this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
//               dtInstance.destroy();
//               this.rewards.push(reward);
//               this.dtTriggerContent.next();
//               this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
//             });
//           });

//       }
//       else
//         this.appComponent.showError("Carrot must be a Number");
//     }
//     else
//       this.appComponent.showError("Please fill all the field");  

//     /*
//     this.dtElementContent.dtInstance.then((dtInstance: DataTables.Api) => {
//       let currentCarrot = 0;

//       if(!isNaN(carrot))
//       {
//         rewardName = rewardName.trim();

//         this.rewardTemp.type = 3;
      
//         this.rewardTemp.role = role;
//         this.rewardTemp.name = rewardName;

//         this.rewardTemp.groupsId = [];
        
//         let groupsId: string[] = new Array<string>();
//         groupsId = this.rewardTemp.groupsId;
//         this.groups.forEach((group) => {
//           groupsId.push(group.id);
//         });
//         this.rewardTemp.groupsId = groupsId;

//         this.rewardTemp.carrot = carrot;
//         this.rewardTemp.currentCarrot = currentCarrot;
//         this.rewardTemp.active = true;

//         this.adminService.addReward(this.rewardTemp)
//           .subscribe(reward => {
//             dtInstance.destroy();
//             this.rewards.push(reward);
//             this.dtTriggerContent.next();
//           });

//         this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
//       }
//       else
//         this.appComponent.showError("Carrot must be a Number");

//     });
//     */
    
//   }

//   addAchievement(role: number, rewardName: string, rewardDesc:string, carrot: number): void 
//   {
//     if(role != null || rewardName != "" || carrot != null)
//     {
//       if(!isNaN(carrot))
//       {
//         rewardName = rewardName.trim();
//         let flag : boolean = true;
  
//         this.rewardTemp.type = 1;
  
//         this.rewardTemp.role = role;
//         this.rewardTemp.name = rewardName;
//         this.rewardTemp.description = rewardDesc;
  
//         this.rewardTemp.groupsId = [];
        
//         let groupsId: string[] = new Array<string>();
//         groupsId = this.rewardTemp.groupsId;
//         this.groups.forEach((group) => {
//           groupsId.push(group.id);
//         });
//         this.rewardTemp.groupsId = groupsId;
  
//         this.rewardTemp.carrot = carrot;
//         this.rewardTemp.active = true;
  
//         this.adminService.addReward(this.rewardTemp)
//           .subscribe(reward => {
//             this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
//               dtInstance.destroy();
//               this.rewards.push(reward);
//               this.dtTriggerContent.next();
//               this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
//             });
//           });   
//       }
//       else
//         this.appComponent.showError("Carrot must be a Number");
//     }
//     else
//       this.appComponent.showError("Please fill all the field");  
//   }

//   addChange(reward: Content, parameterChanged: string, from: string) :Content
//   {
//     reward.changes.push({parameterChanged, from} as ContentChange);

//     return reward;
//   }

//   addGroup(groupName:string)
//   {
//     if(groupName != "")
//     { 
//       if(this.selectedUser.id != undefined && this.selectedUser.id != "" && groupName != "")
//       {
//         let newGroup : Group = new Group;
//         newGroup.name = groupName;
//         newGroup.managerId = this.selectedUser.id;
//         newGroup.membersId = [];
//         newGroup.members = [];
//         newGroup.autoCarrotAdmin = true;
//         newGroup.birthdayCarrotAdmin = 0;
//         if(this.currentGroupChoice == "Employee")
//         {
//           this.groupService.updateGroup(newGroup).subscribe(group => {
//             this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
//               dtInstance.destroy();
//               this.groups.push(group);
//               this.dtTriggerContent.next();
//             });
//           });
//         }
//         else
//         {
//           this.groupService.updateManagerGroup(newGroup).subscribe(group => {
//             this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
//               dtInstance.destroy();
//               this.groups.push(group);
//               this.dtTriggerContent.next();
//             });
//           });
//         }

//         this.appComponent.showSuccess(groupName + " Staff Group has been Added."); 
//       }
//       else
//         this.appComponent.showError("Please select a Manager and fill the group name");
//     }
//   }

  publish(title:string)
  {
    let notifTemp : Notification = new Notification;
    let userTemp : User = this.userService.getLoggedInUser();
    
    if(title != "" && this.editorContent != "")
    {
      notifTemp.type = 2;
      notifTemp.adminId = userTemp.id;
      notifTemp.title = "(Newsletter) " + title;
      notifTemp.description = this.editorContent;
  
      this.notificationService.addEdit(notifTemp).subscribe(notification => {
        // this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
        //   dtInstance.destroy();
        //   this.newsletters.push(notification);
        //   this.dtTriggerNewsletter.next();
        // });
        this.newsletters.push(notification);
        this.appComponent.showSuccess(title + " is now published."); 
      });
    }
    else
      this.appComponent.showError("Please fill the title and the content"); 

  }

//   handleFileInputContent(files: FileList){
//     if (files.length < 1) {
//       return;
//     }

//     this.fileToUpload = files[0];

//     this.userService.uploadFile(this.fileToUpload)
//       .subscribe(res => {
//         if (this.rewardTemp && res) {
//           this.rewardTemp.picture = res.url as string;
//           // this.updateUser();
//         } else {
//           this.appComponent.showError('Failed to upload photo');
//         }
//       });
//   }

  
//   //------------------------------

//   //----------CONTENT ONLY METHOD----------

//   open(reward: Content): void
//   {
//     reward.active = true;
//     this.adminService.addReward(reward).subscribe(reward => {
//       this.appComponent.showSuccess(reward.name + " is now open.");
//     });

//   }

//   close(): void
//   {
//     this.soonToBeClosedReward.active = false;
//     this.adminService.addReward(this.soonToBeClosedReward).subscribe(reward => {
//       let notifTemp : Notification = new Notification;
//       let userTemp : User = this.userService.getLoggedInUser();
//       this.appComponent.showSuccess(reward.name + " is now closed.");
      
//       notifTemp.type = 3;
//       notifTemp.adminId = userTemp.id;
//       notifTemp.title = this.soonToBeClosedReward.name + " is now closed.";
//       notifTemp.description = this.closeMessage + ". \n Closed by " + userTemp.name + ".";
  
//       this.notificationService.addEdit(notifTemp).subscribe(notif =>{
//       });
      
  
//     });

   
//   }

//   delete(reward: Content): void 
//   {
//     this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => { 
//       reward.deleted = true;
//       this.adminService.deleteReward(reward).subscribe(r => {
//         if(r){
//           dtInstance.destroy();
//           this.rewards = this.rewards.filter(r => {
//             return r.id != reward.id;
//           })
//           this.dtTriggerContent.next();
//           this.appComponent.showSuccess(reward.name + " is deleted.");
//         }
//         else
//           this.appComponent.showError("Deleting " + reward.name + " is not succeed.");
//       });

//     });


   
//   }

//   //---------------------------------------

//   //----------ON SELECT METHOD----------

//   onSelect(reward : Content):void
//   {
//     this.selectedReward = reward;

//     this.rewardTemp.id = reward.id;
//     this.rewardTemp.type = reward.type;
//     this.rewardTemp.role = reward.role;
//     this.rewardTemp.name = reward.name;
//     this.rewardTemp.expired = reward.expired;
//     this.rewardTemp.changes = reward.changes;
//     this.rewardTemp.groupsId = reward.groupsId;
//     this.rewardTemp.carrot = reward.carrot;
//     this.rewardTemp.active = reward.active;

//     this.disabledGroup = [];
//     this.enabledGroup = [];

//     this.changes = reward.changes;
  
//     this.groups.forEach((group) => {
//       let flag:boolean = false;
//       for(let i = 0; i< this.selectedReward.groupsId.length; i++)
//       {
//         if(group.id == this.selectedReward.groupsId[i])
//         {
//           this.enabledGroup.push(group);
//           flag = true;
//           break;
//         }
//       }
//       if(flag == false)
//         this.disabledGroup.push(group);
//     });
//   }

  
//   onSelectUser(managerId : string):void
//   {
//     if(managerId != "--SELECT A MANAGER--" && managerId != "")
//     {    
//       this.selectedUser.id = managerId;
//     }
//   }

//   groupOnSelect(group : Group):void
//   {
//     this.selectedGroup = group;

//     this.groupTemp.id = group.id;
//     this.groupTemp.name = group.name;
//     this.groupTemp.managerId = group.managerId;
//     this.groupTemp.membersId = group.membersId;
//     this.groupTemp.autoCarrotAdmin = group.autoCarrotAdmin;
//     this.groupTemp.birthdayCarrotAdmin = group.birthdayCarrotAdmin;
//     this.groupTemp.autoCarrotManager = group.autoCarrotManager;
//     this.groupTemp.birthdayCarrotManager = group.birthdayCarrotManager;
//     this.groupTemp.creationDate = group.creationDate;
//     this.groupTemp.active = group.active;
//     this.groupTemp.manager = group.manager;
//     this.groupTemp.members = group.members;
//   }

//   groupOnSelectToMove(group : Group):void
//   {
//     this.changeGroupCheck = true;
//     this.selectedGroup = group;   
//     this.onEnabledGroup(this.selectedGroup);
//   }
  

//   groupOnSelectToRemove(group:Group)
//   {
//     this.changeGroupCheck = true;
//     this.selectedGroup = group;
//     this.onRemoveGroupToMoves(this.selectedGroup);
//   }

  onSelectNewsletter(newsletter:Notification)
  {
    this.selectedNewsletter = newsletter;
  }

//   onSelectApproval(user:User, approval:Transaction)
//   {
//     this.selectedUser = user;
//     this.selectedApproval = approval;
//   }
//   //------------------------------------

//   //----------EDIT METHOD----------

//   save() 
//   {

//     if(this.rewardTemp.expired != this.selectedReward.expired)
//     {
//       this.selectedReward =  this.addChange(this.selectedReward, "Expired Date", this.rewardTemp.expired)
//     }
//     if(this.rewardTemp.carrot != this.selectedReward.carrot)
//     {
//       this.selectedReward =  this.addChange(this.selectedReward, "Maximum Carrot", this.rewardTemp.carrot.toString())
//     }
    
//     if(this.rewardTemp.name != this.selectedReward.name)
//     {
//       this.selectedReward =  this.addChange(this.selectedReward, "Content Name", this.rewardTemp.name);
//     } 

//     this.selectedReward.groupsId = [];

//     if(this.checkGroupType)
//     {
//       this.enabledGroup.forEach((group) => {
//         this.selectedReward.groupsId.push(group.id);
//         this.selectedReward = this.addChange(this.selectedReward, "Groups Able", group.name);
//       })
      
//       this.disabledGroup.forEach((group) =>{
//         this.selectedReward = this.addChange(this.selectedReward, "Groups Disable", group.name);
//       })
//     }
    
//     this.changeGroupCheck = false;
   
//     this.adminService.addReward(this.selectedReward).subscribe(res => {
//       console.log(res);
//       if(res)
//       {
//         this.selectedReward = res;
//         this.selectedReward.changes[this.selectedReward.changes.length-1].on = new Date().toString();
//       }
//     });

//     this.appComponent.showSuccess("'" + this.selectedReward.name + "'" +  " has been updated.");
//   }

   

//   saveBirthdayStatus()
//   {
//     this.groupService.updateGroup(this.selectedGroup).subscribe();
//     this.appComponent.showSuccess("Birthday Carrot has been Updated"); 
//   }


//   editGroupName():void
//   {
//     this.groupService.updateGroup(this.selectedGroup).subscribe();
//     this.appComponent.showSuccess("Group name now become " + this.selectedGroup.name + "."); 
//   }


//   //-------------------------------
  
//   //----------MOVING STAFF GROUP ON EDIT METHOD----------

//   onEnabledGroup(group: Group)
//   {
//     this.disabledGroup = this.disabledGroup.filter((us) => {
//       return us.id != group.id;
//     });
//     this.enabledGroup.push(group);
//   }

//   onRemoveGroupToMoves(group: Group){
//     this.enabledGroup = this.enabledGroup.filter((man) => {
//       return man.id != group.id
//     });
//     this.disabledGroup.push(group);
//   }
//   //-----------------------------------------------------

//   //----------SET METHOD----------


//   setLate(user: User)
//   {
//     if(user.alwaysLate == true)
//     {    
//       user.alwaysLate = false;    
//       this.appComponent.showSuccess(user.name + " is now flagged 'Not Late'."); 
//     }
//     else
//     {
//       this.appComponent.showSuccess(user.name + " is now flagged 'Late'."); 
//       user.alwaysLate = true;
//     }

//     this.userService.addEdit(user).subscribe();
//   }

//   setClose(reward: Content): void
//   {
//     this.soonToBeClosedReward = reward;
//     this.adminService.addReward(reward).subscribe();
//   }

//   setApprove(message : string)
//   { 
//     console.log(message);
//     if(message != "")
//     {
//       this.selectedApproval.approved = true;
//       this.transactionService.postTransactionReject(this.selectedApproval).subscribe(transactions => {
//         this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
//           dtInstance.destroy(); 
//           this.approvals = [];
//           this.transactions.forEach((transaction) => {
//             if(transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
//               this.approvals.push(transaction);
//           });
//           this.dtTriggerContent.next();
//         });  

//         let notifTemp : Notification = new Notification;
//         notifTemp.adminId = this.loggedInUser.id;
//         notifTemp.userId = this.selectedUser.id;
//         notifTemp.title = "Transaction Approved";
//         notifTemp.description = message;
//         notifTemp.type = 3;
  
//         this.notificationService.addEdit(notifTemp).subscribe(notif =>{
//           if(notif)
//             this.appComponent.showSuccess(this.getUserNameById(this.selectedApproval.senderId)+  "'s transaction is approved.");
//         })
//       });
//     }
//     else
//       this.appComponent.showError("Please fill the message.");
    
//   }

//   setDisapprove(message: string)
//   {
//     if(message != "")
//     {
//       this.selectedApproval.rejected = true;
//       this.transactionService.postTransactionReject(this.selectedApproval).subscribe(transactions =>  {
//         this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
//           dtInstance.destroy(); 
//           this.approvals = [];
//           this.transactions.forEach((transaction) => {
//             if(transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
//               this.approvals.push(transaction);
//           });
//           this.dtTriggerContent.next();
//         });  

//         let notifTemp : Notification = new Notification;
//         notifTemp.adminId = this.loggedInUser.id;
//         notifTemp.userId = this.selectedUser.id;
//         notifTemp.title = "Transaction Rejected";
//         notifTemp.description = message;
//         notifTemp.type = 3;

//         this.notificationService.addEdit(notifTemp).subscribe(notif =>{
//           if(notif)
//           this.appComponent.showSuccess(this.getUserNameById(this.selectedApproval.senderId)+  "'s transaction is rejected.");
//         })
//       });
//     }
//     else
//       this.appComponent.showError("Please fill the message.");
//   }
//   //------------------------------

//   //----------CHECK METHOD----------

//   checkOpen(status)
//   {
//     if(status == true)
//       return true;
//   } 

//   checkClose(status)
//   {
//     if(status == false)
//       return true;
//   }

//   checkContent(type)
//   {
//     this.currentChoice = type.target.value;
//   }
  
//   checkGroupType(type)
//   {
//     this.currentGroupChoice = type.target.value;
//   }

//   //--------------------------------

//   //----------CANCEL METHOD----------

//   cancelSave()
//   {
//     this.selectedReward.name = this.rewardTemp.name;
//     this.selectedReward.expired = this.rewardTemp.expired;
//     this.selectedReward.carrot = this.rewardTemp.carrot;
//   }

//   cancelEdit()
//   {
//     this.selectedGroup.name = this.groupTemp.name;
//     this.selectedGroup.autoCarrotAdmin = this.groupTemp.autoCarrotAdmin;
//     this.selectedGroup.birthdayCarrotAdmin = this.groupTemp.birthdayCarrotAdmin;
//   }

  //---------------------------------

  refresh(): void {
    window.location.reload();
  }
}

