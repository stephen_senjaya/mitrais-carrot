import { Component, OnInit, AfterViewInit, group } from '@angular/core';
import { Router } from "@angular/router";

import { ViewChild } from '@angular/core'
import { NgModule } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CommonModule } from '@angular/common';

import { User } from '../../model/user';
import { Transaction } from '../../model/transaction';
import { Notification } from '../../model/notification';
import { Content } from '../../model/content';

import { AdminService } from '../../service/admin.service';
import { TransactionService } from '../../service/transaction.service';
import { NotificationService } from '../../service/notification.service';

import {AppComponent} from '../../app.component';

import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';

import { UserService } from '../../service/user.service';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs';

import * as $ from 'jquery';
import 'datatables.net';

@Component({
  selector: 'app-approval',
  templateUrl: './approval.component.html',
  styleUrls: ['./approval.component.css']
})
export class ApprovalComponent implements OnInit {
  isLoading: boolean = false;

  users : User [];
  rewards : Content [];

  loggedInUser: User;

  transactions:Transaction[] = [];
  approvals:Transaction[];

  loadingApproval: boolean=true;

  selectedUser : User = new User;
  selectedApproval : Transaction;

  dtOptionsApproval: DataTables.Settings = {};
  dtTriggerApproval: Subject<any> = new Subject();


  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  constructor(
    private http: HttpClient,
    private adminService: AdminService,
    private userService: UserService,
    private transactionService: TransactionService,
    private notificationService: NotificationService,
    private appComponent: AppComponent,
    private router: Router
  ){}

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();

    if(!this.loggedInUser) {
      let token = localStorage.getItem(Constanta.TOKEN_KEY_NAME);
      if (token) {
        this.userService.loginWithToken(token)
          .subscribe(res => {
            if (res) {
              this.loggedInUser = res;
              this.userService.setLoggedInUser(res);
              this.isLoading = false;
              if (!res.admin)
                this.router.navigate(['/']);
            } else
            {
              this.isLoading = false;
              this.router.navigate(['/']);
            }
          })
      } else {
        this.isLoading = false;
        this.router.navigate(['/']);
      }
    }
      else 
        this.isLoading = false;

    this.getTransaction();
    this.getUsers();
  }

  getUsers()
  {
    return this.adminService.getUsers().subscribe(users=> {
      this.users = users
    });
  }
  
  getDate(type: string):string
  {
    return Utility.transformDate(type, 'dd-MM-yyyy');
  }


  getTransaction()
  {
    this.approvals = [];
    this.transactionService.fetchTransactions().subscribe(transactions => {
      this.transactions = transactions;
      this.transactions.forEach((transaction) => {
        if(transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
          this.approvals.push(transaction);
      });   
      this.loadingApproval = false;
      this.dtTriggerApproval.next();
    });
  }

  getUserNameById(id:string):string
  {
    if(this.users)
    {
      let name: string = "";
      this.users.forEach(user => {
        if(user.id == id)
          name = user.name;
      });
      return name;
    }
  }

  getContentById(id:string):string
  {
    if(this.rewards)
    {
      let name: string = "";
      this.rewards.forEach(reward => {
        if(reward.id == id)
          name = reward.name;
      });
      return name;
    }
  }

  onSelectApproval(user:User, approval:Transaction)
  {
    this.selectedUser = user;
    this.selectedApproval = approval;
  }

  setApprove(message : string)
  { 
    console.log(message);
    if(message != "")
    {
      this.selectedApproval.approved = true;
      this.transactionService.postTransactionReject(this.selectedApproval).subscribe(transactions => {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy(); 
          this.approvals = [];
          this.transactions.forEach((transaction) => {
            if(transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
              this.approvals.push(transaction);
          });
          this.dtTriggerApproval.next();
        });  

        let notifTemp : Notification = new Notification;
        notifTemp.adminId = this.loggedInUser.id;
        notifTemp.userId = this.selectedUser.id;
        notifTemp.title = "Transaction Approved";
        notifTemp.description = message;
        notifTemp.type = 3;
  
        this.notificationService.addEdit(notifTemp).subscribe(notif =>{
          if(notif)
            this.appComponent.showSuccess(this.getUserNameById(this.selectedApproval.senderId)+  "'s transaction is approved.");
        })
      });
    }
    else
      this.appComponent.showError("Please fill the message.");
    
  }

  setDisapprove(message: string)
  {
    if(message != "")
    {
      this.selectedApproval.rejected = true;
      this.transactionService.postTransactionReject(this.selectedApproval).subscribe(transactions =>  {
        this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
          dtInstance.destroy(); 
          this.approvals = [];
          this.transactions.forEach((transaction) => {
            if(transaction.type == 2 && transaction.approved == false && transaction.rejected == false)
              this.approvals.push(transaction);
          });
          this.dtTriggerApproval.next();
        });  

        let notifTemp : Notification = new Notification;
        notifTemp.adminId = this.loggedInUser.id;
        notifTemp.userId = this.selectedUser.id;
        notifTemp.title = "Transaction Rejected";
        notifTemp.description = message;
        notifTemp.type = 3;

        this.notificationService.addEdit(notifTemp).subscribe(notif =>{
          if(notif)
          this.appComponent.showSuccess(this.getUserNameById(this.selectedApproval.senderId)+  "'s transaction is rejected.");
        })
      });
    }
    else
      this.appComponent.showError("Please fill the message.");
  }

  refresh(): void {
    window.location.reload();
  }
}
