import { Component, OnInit, AfterViewInit, group } from '@angular/core';
import { Router } from "@angular/router";

import { ViewChild } from '@angular/core'
import { NgModule } from '@angular/core';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Location } from '@angular/common';
import { CommonModule } from '@angular/common';
import { DatePipe } from '@angular/common';

import { Content } from '../../model/content';
import { ContentChange } from '../../model/content-change';
import { User } from '../../model/user';
import { Group } from '../../model/group';
import { Transaction } from '../../model/transaction';
import { Notification } from '../../model/notification';

import { AdminService } from '../../service/admin.service';
import { GroupService } from '../../service/group.service';
import { TransactionService } from '../../service/transaction.service';
import { NotificationService } from '../../service/notification.service';

import {AppComponent} from '../../app.component';

import * as Utility from '../../config/utility';
import * as Constanta from '../../config/constanta';

import { forEach } from '@angular/router/src/utils/collection';
import { OrderPipe } from 'ngx-order-pipe';
import { UserService } from '../../service/user.service';
import { DataTableDirective } from 'angular-datatables';

import { Subject } from 'rxjs';

import * as $ from 'jquery';
import 'datatables.net';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {

  changes : ContentChange [];
  rewards : Content [];
  groups : Group [];
  loggedInUser: User;

  disabledGroup : Group[] = [];
  enabledGroup : Group[] = [];
  
  fileToUpload: File = null;

  rewardTemp: Content = new Content();

  limit:number;
  expired:Date;

  currentChoice:string = "Reward";
  currentGroupChoice:string = "Employee";

  changeGroupCheck:boolean = false;

  selectedReward : Content;
  selectedGroup : Group;

  soonToBeClosedReward : Content;
  closeMessage: string = "";
  

  dtOptionsContent: DataTables.Settings = {};
  dtTriggerContent: Subject<any> = new Subject();


  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  constructor(
    private http: HttpClient,
    private adminService: AdminService,
    private groupService: GroupService,
    private userService: UserService,
    private notificationService: NotificationService,
    private appComponent: AppComponent,
    private router: Router
  ){}

  ngOnInit() 
  {
    this.loggedInUser = this.userService.getLoggedInUser();

    this.getRewards();
    this.getGroups();
  }

  getRewards(): void 
  {
    this.adminService.getRewards().subscribe(rewards=> {
      this.rewards = rewards;
      this.dtTriggerContent.next();
    });
  }

  getGroups()
  {
    this.groups = [];
    return this.groupService.getAllGroups().subscribe(groups => {
        this.groups = groups;
        this.groupService.getAllManagerGroups().subscribe(mGroups =>{
          mGroups.forEach((group) =>{
            this.groups.push(group);
          });
        });
      });
  }

  getRewardType(type : number):string
  {
    return Utility.getType(type);
  }

  getDate(type: string):string
  {
    return Utility.transformDate(type, 'dd-MM-yyyy');
  }

  getStatus(active : boolean):string
  {
    if(active == true)
      return "Open";
    else
      return "Closed";
  }

  getRoleType(type : number):string
  {
    return Utility.getRole(type);
  }

  getContentById(id:string):string
  {
    if(this.rewards)
    {
      let name: string = "";
      this.rewards.forEach(reward => {
        if(reward.id == id)
          name = reward.name;
      });
      return name;
    }
  }

  addReward(role: number, rewardName: string, rewardDesc:string, carrot: number): void 
  {
    if(role != null || rewardName != "" || carrot != null)
    {
      let rewardLimit = this.limit;
      let expiredDate = this.expired;
  
      if(!isNaN(carrot))
      {
        rewardName = rewardName.trim();
  
        this.rewardTemp.type = 2;
        if(!isNaN(rewardLimit) && rewardLimit && expiredDate)
        {
          this.rewardTemp.currentCarrot = rewardLimit;
          this.rewardTemp.expired = expiredDate.toString();
  
          this.rewardTemp.role = role;
          this.rewardTemp.name = rewardName;
          this.rewardTemp.description = rewardDesc;
    
          this.rewardTemp.groupsId = [];
          this.rewardTemp.changes = [];
          
          let groupsId: string[] = new Array<string>();
          groupsId = this.rewardTemp.groupsId;
          this.groups.forEach((group) => {
            groupsId.push(group.id);
          });
          this.rewardTemp.groupsId = groupsId;
    
          this.rewardTemp.carrot = carrot;
          this.rewardTemp.active = true;
    
          this.adminService.addReward(this.rewardTemp)
            .subscribe(reward =>{
              if(reward)
              {
                this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
                this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
                  dtInstance.destroy();
                  this.rewards.push(reward);
                  this.dtTriggerContent.next();
                });
              }
              else
                this.appComponent.showSuccess("Sending Failed.");
            });
    
        }
        else
          this.appComponent.showError("Redeem Limit must be number and please Fill the date and Redeem Limit")
      }
      else
        this.appComponent.showError("Carrot must be a Number");     
    }
    else
      this.appComponent.showError("Please fill all the field");  
  }

  addSocial(role: number, rewardName: string, rewardDesc:string, carrot: number): void 
  {  
    if(role != null || rewardName != "" || carrot != null)
    {
      let currentCarrot = 0;

      if(!isNaN(carrot))
      {
        rewardName = rewardName.trim();

        this.rewardTemp.type = 3;
      
        this.rewardTemp.role = role;
        this.rewardTemp.name = rewardName;
        this.rewardTemp.description = rewardDesc;

        this.rewardTemp.groupsId = [];
        this.rewardTemp.changes = [];
        
        let groupsId: string[] = new Array<string>();
        groupsId = this.rewardTemp.groupsId;
        this.groups.forEach((group) => {
          groupsId.push(group.id);
        });
        this.rewardTemp.groupsId = groupsId;

        this.rewardTemp.carrot = carrot;
        this.rewardTemp.currentCarrot = currentCarrot;
        this.rewardTemp.active = true;

        this.adminService.addReward(this.rewardTemp)
          .subscribe(reward => {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.rewards.push(reward);
              this.dtTriggerContent.next();
              this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
            });
          });

      }
      else
        this.appComponent.showError("Carrot must be a Number");
    }
    else
      this.appComponent.showError("Please fill all the field");  
  }

  addAchievement(role: number, rewardName: string, rewardDesc:string, carrot: number): void 
  {
    if(role != null || rewardName != "" || carrot != null)
    {
      if(!isNaN(carrot))
      {
        rewardName = rewardName.trim();
        let flag : boolean = true;
  
        this.rewardTemp.type = 1;
  
        this.rewardTemp.role = role;
        this.rewardTemp.name = rewardName;
        this.rewardTemp.description = rewardDesc;
  
        this.rewardTemp.groupsId = [];
        this.rewardTemp.changes = [];
        
        let groupsId: string[] = new Array<string>();
        groupsId = this.rewardTemp.groupsId;
        this.groups.forEach((group) => {
          groupsId.push(group.id);
        });
        this.rewardTemp.groupsId = groupsId;
  
        this.rewardTemp.carrot = carrot;
        this.rewardTemp.active = true;
  
        this.adminService.addReward(this.rewardTemp)
          .subscribe(reward => {
            this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.rewards.push(reward);
              this.dtTriggerContent.next();
              this.appComponent.showSuccess("'" + rewardName + "'" +  " is added.");
            });
          });   
      }
      else
        this.appComponent.showError("Carrot must be a Number");
    }
    else
      this.appComponent.showError("Please fill all the field");  
  }

  addChange(reward: Content, parameterChanged: string, from: string) :Content
  {
    reward.changes.push({parameterChanged, from} as ContentChange);

    return reward;
  }

  handleFileInputContent(files: FileList){
    if (files.length < 1) {
      return;
    }

    this.fileToUpload = files[0];

    this.userService.uploadFile(this.fileToUpload)
      .subscribe(res => {
        if (this.rewardTemp && res) {
          this.rewardTemp.picture = res.url as string;
          // this.updateUser();
        } else {
          this.appComponent.showError('Failed to upload photo');
        }
      });
  }

  open(reward: Content): void
  {
    reward.active = true;
    this.adminService.addReward(reward).subscribe(reward => {
      this.appComponent.showSuccess(reward.name + " is now open.");
    });

  }

  close(): void
  {
    this.soonToBeClosedReward.active = false;
    this.adminService.addReward(this.soonToBeClosedReward).subscribe(reward => {
      let notifTemp : Notification = new Notification;
      let userTemp : User = this.userService.getLoggedInUser();
      this.appComponent.showSuccess(reward.name + " is now closed.");
      
      notifTemp.type = 3;
      notifTemp.adminId = userTemp.id;
      notifTemp.title = this.soonToBeClosedReward.name + " is now closed.";
      notifTemp.description = this.closeMessage + ". \n Closed by " + userTemp.name + ".";
  
      this.notificationService.addEdit(notifTemp).subscribe(notif =>{
      });
    });
  }

  delete(reward: Content): void 
  {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => { 
      reward.deleted = true;
      this.adminService.deleteReward(reward).subscribe(r => {
        if(r){
          dtInstance.destroy();
          this.rewards = this.rewards.filter(r => {
            return r.id != reward.id;
          })
          this.dtTriggerContent.next();
          this.appComponent.showSuccess(reward.name + " is deleted.");
        }
        else
          this.appComponent.showError("Deleting " + reward.name + " is not succeed.");
      });
    });
  }

  onSelect(reward : Content):void
  {
    this.selectedReward = reward;

    this.rewardTemp.id = reward.id;
    this.rewardTemp.type = reward.type;
    this.rewardTemp.role = reward.role;
    this.rewardTemp.name = reward.name;
    this.rewardTemp.expired = reward.expired;
    this.rewardTemp.changes = reward.changes;
    this.rewardTemp.groupsId = reward.groupsId;
    this.rewardTemp.carrot = reward.carrot;
    this.rewardTemp.active = reward.active;

    this.disabledGroup = [];
    this.enabledGroup = [];

    this.changes = reward.changes;
  
    this.groups.forEach((group) => {
      let flag:boolean = false;
      for(let i = 0; i< this.selectedReward.groupsId.length; i++)
      {
        if(group.id == this.selectedReward.groupsId[i])
        {
          this.enabledGroup.push(group);
          flag = true;
          break;
        }
      }
      if(flag == false)
        this.disabledGroup.push(group);
    });
  }

  save() 
  {

    if(this.rewardTemp.expired != this.selectedReward.expired)
    {
      this.selectedReward =  this.addChange(this.selectedReward, "Expired Date", this.rewardTemp.expired)
    }
    if(this.rewardTemp.carrot != this.selectedReward.carrot)
    {
      this.selectedReward =  this.addChange(this.selectedReward, "Maximum Carrot", this.rewardTemp.carrot.toString())
    }
    
    if(this.rewardTemp.name != this.selectedReward.name)
    {
      this.selectedReward =  this.addChange(this.selectedReward, "Content Name", this.rewardTemp.name);
    } 

    this.selectedReward.groupsId = [];

    if(this.checkGroupType)
    {
      this.enabledGroup.forEach((group) => {
        this.selectedReward.groupsId.push(group.id);
        this.selectedReward = this.addChange(this.selectedReward, "Groups Able", group.name);
      })
      
      this.disabledGroup.forEach((group) =>{
        this.selectedReward = this.addChange(this.selectedReward, "Groups Disable", group.name);
      })
    }
    
    this.changeGroupCheck = false;
   
    this.adminService.addReward(this.selectedReward).subscribe(res => {
      console.log(res);
      if(res)
      {
        this.selectedReward = res;
        this.selectedReward.changes[this.selectedReward.changes.length-1].on = new Date().toString();
      }
    });

    this.appComponent.showSuccess("'" + this.selectedReward.name + "'" +  " has been updated.");
  }

  groupOnSelectToMove(group : Group):void
  {
    this.changeGroupCheck = true;
    this.selectedGroup = group;   
    this.onEnabledGroup(this.selectedGroup);
  }
  
  groupOnSelectToRemove(group:Group)
  {
    this.changeGroupCheck = true;
    this.selectedGroup = group;
    this.onRemoveGroupToMoves(this.selectedGroup);
  }

  onEnabledGroup(group: Group)
  {
    this.disabledGroup = this.disabledGroup.filter((us) => {
      return us.id != group.id;
    });
    this.enabledGroup.push(group);
  }

  onRemoveGroupToMoves(group: Group){
    this.enabledGroup = this.enabledGroup.filter((man) => {
      return man.id != group.id
    });
    this.disabledGroup.push(group);
  } 

  setLate(user: User)
  {
    if(user.alwaysLate == true)
    {    
      user.alwaysLate = false;    
      this.appComponent.showSuccess(user.name + " is now flagged 'Not Late'."); 
    }
    else
    {
      this.appComponent.showSuccess(user.name + " is now flagged 'Late'."); 
      user.alwaysLate = true;
    }

    this.userService.addEdit(user).subscribe();
  }

  setClose(reward: Content): void
  {
    this.soonToBeClosedReward = reward;
    this.adminService.addReward(reward).subscribe();
  }

  checkOpen(status)
  {
    if(status == true)
      return true;
  } 

  checkClose(status)
  {
    if(status == false)
      return true;
  }

  checkContent(type)
  {
    this.currentChoice = type.target.value;
  }
  
  checkGroupType(type)
  {
    this.currentGroupChoice = type.target.value;
  }
  
  refresh(): void {
    window.location.reload();
  }

}
