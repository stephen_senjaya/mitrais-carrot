import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { Config } from '../model/config';

import * as Constanta from '../config/constanta';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ConfigService {

  constructor(
    private http: HttpClient) { }

  
  private configUrl = Constanta.API_URL + 'config';

  config: Config = null;

  getConfigData(): Config{
    if (!this.config){
      this.getConfig().subscribe(res => {
        if (res) {
          console.log(res);
          this.config = res[0];
        }
        return this.config;
      });
    } else
      return this.config;
  }
    
  getConfig(): Observable<Config[]> 
  {
    return this.http.get<Config[]>(this.configUrl);
  }
  
  updateConfig(config: Config): Observable<Config> 
  {
    return this.http.post<Config>(this.configUrl, config, httpOptions);
  }

}
