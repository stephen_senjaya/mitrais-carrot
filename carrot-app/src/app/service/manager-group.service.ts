import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { ManagerGroup } from "../model/manager-group";
import { User } from '../model/user';
import { Group } from '../model/group';
import * as Constanta from '../config/constanta';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ManagerGroupService {
  private manGroupsUrl = Constanta.API_URL + 'manager-group';
  private fetchManGroupsBySMIDUrl = Constanta.API_URL + 'manager-group/supervisor/';

  constructor(private http: HttpClient) { }

  getAllManGroups(): Observable<ManagerGroup[]> {
    try {
      let res = this.http.get<ManagerGroup[]>(this.manGroupsUrl);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  // removeManagerFromGroup (managerGroup: ManagerGroup): ManagerGroup 
  // {
  //   try {
  //     this.http.post<ManagerGroup>(this.manGroupsUrl, managerGroup, httpOptions).subscribe(res => {
  //       return res;
  //     });      
  //   } catch (error) {
  //     return error;
  //   }
  // }

  addEdit(managerGroup: ManagerGroup): Observable<ManagerGroup>
  {
    try {
      let res = this.http.post<ManagerGroup>(this.manGroupsUrl, managerGroup, httpOptions);
      return res;
    } catch (error) {
      return error;
      
    }
  }

  // addManager(managerGroup: ManagerGroup): ManagerGroup
  // {
  //   try {
  //     this.http.post<ManagerGroup>(this.manGroupsUrl, managerGroup, httpOptions).subscribe(res =>{
  //       return res;
  //     });
  //   } catch (error) {
  //     return error;
      
  //   }
  // }

  fetchMyGroup(manID : string): Observable<ManagerGroup[]> {
    try {
      let res = this.http.get<ManagerGroup[]>(this.fetchManGroupsBySMIDUrl + manID);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
