import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { User } from '../model/user';
import * as Constanta from '../config/constanta';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class ManagerService {
  private getAllManUrl = Constanta.API_URL + 'user/manager';
  private getUGMans = Constanta.API_URL + 'user/ungroup-manager';

  constructor(private http: HttpClient) { }

  getAllManager(): Observable<User[]> {
    try {
      let res = this.http.get<User[]>(this.getAllManUrl);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }

  getUnGroupManagers(): Observable<User[] >{
    try {
      let res = this.http.get<User[]>(this.getUGMans);
      console.log(res);
      return res;
    } catch (error) {
      console.log(error);
      return error;
    }
  }
}
