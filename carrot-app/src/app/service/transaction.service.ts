import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';
import { catchError, map, tap } from 'rxjs/operators';

import { Transaction } from '../model/transaction';
import * as Utility from '../config/utility';
import * as Constanta from '../config/constanta';
import { User } from '../model/user';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class TransactionService {

  private transactionsUrl = Constanta.API_URL + 'transaction';
  private transactionsUrlReject = Constanta.API_URL + 'transaction/approve' ;

  transactions: Transaction[];

  constructor(private http: HttpClient) { 
    
    

  }

  // getAll(): Transaction[] {
  //     return this.transactions;
  // }

  // setAll(transactions: Transaction[]){
  //   this.transactions = transactions;
  // }

  fetchTransactions(): Observable<Transaction[]> {
    try {
      let res = this.http.get<Transaction[]>(this.transactionsUrl);
      return res;
    } catch (error) {
      return error;
    }
  }

  fetchTransactionsByUser(userId: string): Observable<Transaction[]> {
    /**
     * for not logged in user
     */
    if(!userId)
      return this.fetchTransactions();

    try {
      let res = this.http.get<Transaction[]>(this.transactionsUrl + '/user/' + userId);
      return res;
    } catch (error) {
      return error;
    }
  }

  postTransaction(transaction: Transaction): Observable<Transaction> {
    try {
      let res = this.http.post<Transaction>(this.transactionsUrl, transaction, httpOptions);
      return res;
    } catch (error) {
      return error;
    }
  }

  postTransactionReject(transaction: Transaction): Observable<Transaction> {
    try {
      let res = this.http.post<Transaction>(this.transactionsUrlReject, transaction, httpOptions);
      return res;
    } catch (error) {
      return error;
    }
  }

}
