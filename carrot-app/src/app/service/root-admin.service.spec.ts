import { TestBed, inject } from '@angular/core/testing';

import { RootAdminService } from './root-admin.service';

describe('RootAdminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RootAdminService]
    });
  });

  it('should be created', inject([RootAdminService], (service: RootAdminService) => {
    expect(service).toBeTruthy();
  }));
});
