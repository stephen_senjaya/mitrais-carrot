import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

import { User } from '../model/user';
import { Config } from '../model/config';
import * as Constanta from '../config/constanta';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable()
export class RootAdminService 
{
  private userUrl = Constanta.API_URL + 'user';
  //private approvalUrl = 'api/rootAdminApprovals';

  constructor(
    private http: HttpClient) { }

  getUsers(): Observable<User[]> 
  {
    return this.http.get<User[]>(this.userUrl);
  }

  updateUser (user: User): Observable<User> 
  {
    return this.http.post<User>(this.userUrl, user, httpOptions);
  }  


}
