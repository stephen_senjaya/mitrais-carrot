import { User } from "./user";

export class Group {
  id: string;
  name: string;
  managerId: string;
  membersId: string[];
  birthdayCarrotAdmin: number;
  autoCarrotAdmin: boolean;
  birthdayCarrotManager: number;
  autoCarrotManager: boolean;
  
  creationDate: string;
  active: boolean;

  manager: User;
  members: User[];
}