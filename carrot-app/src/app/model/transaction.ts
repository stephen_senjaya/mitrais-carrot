import { User } from "./user";

export class Transaction {
  id: string;
  senderId: string;
  receiverId: string;
  contentId: string;
  amount: number;
  description : string;
  type : number;
  dateOccured: string;
  approved : boolean;
  rejected : boolean;
  
  sender: User;
  receiver: User;
}