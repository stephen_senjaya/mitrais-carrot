import { ContentChange } from './content-change'
import { Group } from './group';

export class Content {
    id: string;
    type: number;
    name: string;
    description: string;
    picture: string;
    role: number;
    expired: string;
    /**
     * price for exchange carrot to reward
     * total carrot needed to social
     * amount of achievement
     */
    carrot: number;
    /**
     * for storing amount of receiving carrot now with cutted if more than carrot (social)
     * for storing limit of redeemed (reward)
     * > 0 mean logged in user has accomplished it
     */
    currentCarrot: number;
    /**
     * for storing amount of receiving carrot total (social)
     * for storing times redeemed reward (reward)
     * for storing achieve each month (achievement)
     */
    totalUsage: number;

    changes: ContentChange[] = new Array<ContentChange>();
    groupsId: string[] = new Array<string>();
    creationDate: string;
    active: boolean;
    deleted: boolean;
  }